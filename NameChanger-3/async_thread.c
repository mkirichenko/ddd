#include <wdm.h>
#include "async_context_switcher.h"
#include "async_queue.h"
#include "async_thread.h"


#define ASYNC_QUEUE_TAG 'asyq'


static queue_t *queue = NULL;

static queue_element_t *ended_element = NULL;

static context_t *thread_context = NULL;

static HANDLE thread_handle;


static void async_thread_body(void *argument) {
	UNREFERENCED_PARAMETER(argument);
	context_t context;
	async_context_prepare_initial(&context);
	thread_context = &context;

	queue_element_t *element;
	for (;;) {
		if (ended_element != NULL) {
			async_finalize_context(ended_element->context);
			queue_remove(queue, ended_element);
			ended_element = NULL;
		}
		element = queue_get(queue);
		if (element->context == NULL) {
			/* create & switch context to element->context*/
			async_initial_context_switch(thread_context, element);
		}
		else {
			/* switch context */
			async_context_switch(thread_context, element->context);
		}
	}
}

static void async_initialize(void) {
	queue = (queue_t*)ExAllocatePoolWithTag(PagedPool, sizeof(queue_t), ASYNC_QUEUE_TAG);
	queue_initialize(queue);
	
	NTSTATUS status;
	OBJECT_ATTRIBUTES thread_attributes;
	UNICODE_STRING thread_name;
	RtlInitUnicodeString(&thread_name, L"coroutines_queue_thread");
	InitializeObjectAttributes(&thread_attributes, &thread_name, OBJ_KERNEL_HANDLE, NULL, NULL);
	status = PsCreateSystemThread(&thread_handle, 
		DELETE, 
		NULL, //&thread_attributes, 
		NULL, 
		NULL, 
		async_thread_body, 
		NULL
	);
	if (status != STATUS_SUCCESS) {
		/* bad */
	}
}

void async_queue_task(coroutine_body_t body, void *argument) {
	if (queue == NULL) {
		async_initialize();
	}
	queue_put(queue, body, argument);
}
	
void async_yield(void) {
	if (ended_element != NULL) {
		async_finalize_context(ended_element->context);
		queue_remove(queue, ended_element);
		ended_element = NULL;
	}
	queue_element_t *next_element = queue_get(queue);
	queue_element_t *current_element = next_element->prev;
	if (current_element == NULL) {
		return;
	} else {
		if (next_element->context == NULL) {
			async_initial_context_switch(current_element->context, next_element);
		} else {
			async_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_return(void) {
	if (ended_element != NULL) {
		async_finalize_context(ended_element->context);
		queue_remove(queue, ended_element);
		ended_element = NULL;
	}
	queue_element_t *next_element = queue_get(queue);
	queue_element_t *current_element = next_element->prev;
	if (current_element == NULL) {
		/* last element in queue -> switch to thread context*/
		ended_element = next_element;
		context_t *current_context = next_element->context;
		async_final_context_switch(current_context, thread_context);
	} else {
		ended_element = current_element;
		if (next_element->context == NULL) {
			async_final_initial_context_switch(current_element->context, next_element);
		} else {
			async_final_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_deinitialize(void) {
	NTSTATUS status;
	status = ZwClose(thread_handle);
	if (status != STATUS_SUCCESS) {
		/* bad */
	}
	if (queue != NULL) {
		ExFreePoolWithTag(queue, ASYNC_QUEUE_TAG);
		queue = NULL;
	}
	if (ended_element != NULL) {
		async_finalize_context(ended_element->context);
		queue_release_element(ended_element);
		ended_element = NULL;
	}
}
