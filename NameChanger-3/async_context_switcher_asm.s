; context_t
; size:
;   0x60 (96) bytes
; 	:
;   stack* - 0x0
;   ret_address - 0x8
;   state - 0x10
;   rbx - 0x18
;   rsi - 0x20
;   rdi - 0x28
;   rbp - 0x30
;   rsp - 0x38
;   r12 - 0x40
;   r13 - 0x48
;   r14 - 0x50
;   r15 - 0x58
	
; queue_element_t
; size:
;   0x28 (40) bytes
; 	:
;   body - 0x0
;   argument - 0x8
;   context* - 0x10
;   next - 0x18
;   prev - 0x20

; coroutine stack 
;					addr 
;	-------------   0x0000
;	| available |
;	|   space   |
;   -------------   0x0fa0   
;   | coroutine |
;	|  context  |
;	-------------   0x1000
;



.CODE

async_initial_context_switch_asm PROC FRAME
	.ENDPROLOG
	; arguments:	
	; rcx - from_context*
	; rdx - queue_element*
	; r8 - to_stack*
	; return:
	; nothing
	
	; saving registers
	mov [rcx+018h], rbx
	mov [rcx+020h], rsi
	mov [rcx+028h], rdi
	mov [rcx+030h], rbp
	mov [rcx+038h], rsp
	mov [rcx+040h], r12
	mov [rcx+048h], r13
	mov [rcx+050h], r14
	mov [rcx+058h], r15
	
	; saving return address in from_context
	lea r13, returning
	mov [rcx+08h], r13
	
	; creating to_context
	mov r12, r8
	; saving stack pointer in to_context
	mov [r12+0fa0h], r8
		
	; loading new stack pointers
	lea rsp, [r8+0fa0h]
	lea rbp, [r8+0fa0h]
		
	; saving to_context in queue_element
	mov r11, rdx
	lea r13, [r12+0fa0h]
	mov [r11+010h], r13
	
	; preparing arguments for to_function
	; rcx - to_arg*
	mov rcx, [r11+08h]

	; jumping in to_function
	mov r12, [r11]
	jmp r12

returning:
	ret
	
async_initial_context_switch_asm ENDP




async_context_switch_asm PROC FRAME
	.ENDPROLOG
	; arguments:	
	; rcx - from_context
	; rdx - to_context
	
	; saving registers
	mov [rcx+018h], rbx
	mov [rcx+020h], rsi
	mov [rcx+028h], rdi
	mov [rcx+030h], rbp
	mov [rcx+038h], rsp
	mov [rcx+040h], r12
	mov [rcx+048h], r13
	mov [rcx+050h], r14
	mov [rcx+058h], r15
	
	;saving return address in from_context
	lea r13, returning
	mov [rcx+08h], r13
	
	;loading registers
	mov rbx, [rdx+018h]
	mov rsi, [rdx+020h]
	mov rdi, [rdx+028h]
	mov rbp, [rdx+030h]
	mov rsp, [rdx+038h]
	mov r12, [rdx+040h]
	mov r13, [rdx+048h]
	mov r14, [rdx+050h]
	mov r15, [rdx+058h]
		
	; jumping to return address in to_context
	mov r11, [rdx+08h]
	jmp r11
	
returning:
	ret
	
async_context_switch_asm ENDP




async_final_context_switch_asm PROC FRAME
	.ENDPROLOG
	; arguments:	
	; rcx - to_context

	;loading registers
	mov rbx, [rcx+018h]
	mov rsi, [rcx+020h]
	mov rdi, [rcx+028h]
	mov rbp, [rcx+030h]
	mov rsp, [rcx+038h]
	mov r12, [rcx+040h]
	mov r13, [rcx+048h]
	mov r14, [rcx+050h]
	mov r15, [rcx+058h]

	; jumping to return address in to_context
	mov r11, [rcx+08h]
	jmp r11

returning:
	ret

async_final_context_switch_asm ENDP




async_final_initial_context_switch_asm PROC FRAME
	.ENDPROLOG
	; arguments
	; rcx - queue_element
	; rdx - to_stack
	
	; creating to_context
	mov r12, rdx
	; saving stack pointer in to_context
	mov [r12+0fa0h], rdx
	
	; loading new stack pointers
	lea rsp, [rdx+0fa0h]
	lea rbp, [rdx+0fa0h]
	
	; saving to_context in queue_element
	mov r11, rcx
	lea r13, [r12+0fa0h]
	mov [r11+010h], r13
	
	; preparing arguments for to_function
	; rcx - to_arg*
	mov rcx, [r11+08h]

	; jumping in to_function
	mov r12, [r11]
	jmp r12

returning:
	ret
	
async_final_initial_context_switch_asm ENDP

END

