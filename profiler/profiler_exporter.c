#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler.h"
#include "profiler_exporter.h"

#define PROFILER_MESSAGE_BUFFER_SIZE 1024

void profiler_exporter_header(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char header[2056] = "<html> <head> <style>body {background-color: white;font-family: \"Helvetica\";}h1 {font-style: normal;background-color: #d5d5d5;margin: 10px;border-radius: 10px;margin-left: 40px;margin-right: 40px;text-align: center;}input {cursor: pointer;height: 20px;width: 20px;margin-top: 3px;margin-right: 3px;margin-bottom: 3px;margin-left: 3px;align-content: center; vertical-align:middle; }input ~ ul {display:none;}input:checked ~ ul{display:block;}.arrow-down {width: 0;height: 0;border-style: solid;border-width: 17.3px 10px 0px 10px;border-color: #007bff transparent transparent transparent;}ul.tree {margin-left: 40px;margin-right: 40px;list-style: none;padding-left: 0;margin-top: 16px;margin-bottom: 16px;}ul.tree ul {list-style: none;position: relative;padding-left: 26px;}ul.tree li ul::after {content: \" \";position: absolute;left: 10px;border-left: 1px dashed black;height: 100%;width: 0px;top: 2px;}.checkbock-replacement {height: 20px;width: 20px;opacity: 0;display: inline-block;margin-top: 3px;margin-right: 3px;margin-bottom: 3px;margin-left: 3px;vertical-align: middle;}.tree-node {font-size: 16px;text-align: left;margin: 3px;display: inline-block;vertical-align: middle;height: 20px;}.tree-node-msg {padding: 2px;font-size: 15px;font-family: monospace;display: inline;border-radius: 2px;padding: 2px;}.tree-node-msg {background-color: #a6bed5;}.tree-node-time {display: inline;border-radius: 2px;background-color: inherit;padding: 2px;}.tree-node-time:hover {background-color: #e68585;}.tree-node-file {display: inline;border-radius: 2px;background-color: inherit;padding: 2px;}.tree-node-file {background-color: #e9dc6a;}.tree-node-thread {display: inline;padding: 2px;border-radius: 2px;background-color: #e2e2e2;}.tree-node-timestamp {display: inline;padding: 2px;margin-left: 0px;border-radius: 2px;background-color: #85cc7f;}.title {margin: 0px 40px;font-size: 24px;}ul.list {margin: 0px 40px;list-style: none;padding-left: 0px;margin-top: 16px;margin-bottom: 16px;}</style><title>Profiler Report</title></head><body><h1>Profiler Report</h1>";
		size_t header_size = 2055;
		profiler_write_message_to_log_file(log_file, header, header_size);
	} else if (export_type == TEXT) {
		return;
	}
}

void profiler_exporter_before_thread_trees(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char title[33] = "<div class=\"title\">Threads</div>";
		size_t title_size = 32;
		profiler_write_message_to_log_file(log_file, title, title_size);
	} else if (export_type == TEXT) {
		char title[9] = "Threads\n";
		size_t title_size = 8;
		profiler_write_message_to_log_file(log_file, title, title_size);
	}
}

void profiler_exporter_after_thread_trees(HANDLE log_file, export_type_t export_type) {
	UNREFERENCED_PARAMETER(log_file);
	UNREFERENCED_PARAMETER(export_type);
}

void profiler_exporter_thread_tree_header(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char tree_header[18] = "<ul class=\"tree\">";
		size_t tree_header_size = 17;
		profiler_write_message_to_log_file(log_file, tree_header, tree_header_size);
	}
}

void profiler_exporter_thread_tree_footer(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char tree_footer[6] = "</ul>";
		size_t tree_footer_size = 5;
		profiler_write_message_to_log_file(log_file, tree_footer, tree_footer_size);
	}
}

void profiler_exporter_thread_tree_node_header(HANDLE log_file, export_type_t export_type, message_tree_node_t *node, int level) {
	if (export_type == HTML) {
		char node_buffer[PROFILER_MESSAGE_BUFFER_SIZE];
		size_t node_buffer_size = PROFILER_MESSAGE_BUFFER_SIZE;

		char *open_tag = "<li>";
		size_t open_tag_size = 4;
		profiler_write_message_to_log_file(log_file, open_tag, open_tag_size);

		if (node->children != NULL) {
			char *checkbox = "<input type=\"checkbox\" checked>";
			size_t checkbox_size = 31;
			profiler_write_message_to_log_file(log_file, checkbox, checkbox_size);
		} else {
			char *checkbox = "<div class=\"checkbock-replacement\"></div>";
			size_t checkbox_size = 41;
			profiler_write_message_to_log_file(log_file, checkbox, checkbox_size);
		}

		char *open_node_tag = "<div class=\"tree-node\">";
		size_t open_node_tag_size = 23;
		profiler_write_message_to_log_file(log_file, open_node_tag, open_node_tag_size);

		if (node->parent == NULL) {
			RtlStringCbPrintfA(node_buffer, node_buffer_size, "<div class=\"tree-node-thread\">[%p]</div>", (HANDLE)node->message);
		} else {
			RtlStringCbPrintfA(node_buffer, node_buffer_size, "<div class=\"tree-node-msg\">%s</div>@<div class=\"tree-node-file\">%s:%d</div> real:<div class=\"tree-node-time\">%lld&micro;s</div> CPU:<div class=\"tree-node-time\">%lld&micro;s</div>", node->message, node->file, node->line, node->real_time, node->cpu_time);
		}
		size_t node_length;
		RtlStringCbLengthA(node_buffer, node_buffer_size, &node_length);
		profiler_write_message_to_log_file(log_file, node_buffer, node_length);

		char *close_node_tag = "</div>";
		size_t close_node_tag_size = 6;
		profiler_write_message_to_log_file(log_file, close_node_tag, close_node_tag_size);

		if (node->children != NULL) {
			char *open_list_tag = "<ul>";
			size_t open_list_tag_size = 4;
			profiler_write_message_to_log_file(log_file, open_list_tag, open_list_tag_size);
		}
	} else if (export_type == TEXT) {
		char node_buffer[PROFILER_MESSAGE_BUFFER_SIZE];
		char *leveled_buffer = node_buffer;
		for (int i = 0; i < level; ++i) {
			leveled_buffer[i] = ' ';
		}
		leveled_buffer += level;

		size_t buffer_size = PROFILER_MESSAGE_BUFFER_SIZE - level;
		size_t node_size;
		if (node->parent == NULL) {
			RtlStringCbPrintfA(leveled_buffer, buffer_size, "[%p] root\n", (HANDLE)node->message);
		} else {
			RtlStringCbPrintfA(leveled_buffer, buffer_size, "%s; at %s:%d; real:%lldus cpu:%lldus\n", node->message, node->file, node->line, node->real_time, node->cpu_time);
		}
		RtlStringCbLengthA(node_buffer, PROFILER_MESSAGE_BUFFER_SIZE, &node_size);
		profiler_write_message_to_log_file(log_file, node_buffer, node_size);
	}
}

void profiler_exporter_thread_tree_node_footer(HANDLE log_file, export_type_t export_type, message_tree_node_t *node, int level) {
	UNREFERENCED_PARAMETER(level);
	if (export_type == HTML) {
		if (node->children != NULL) {
			char node_footer[6] = "</ul>";
			size_t node_footer_size = 5;
			profiler_write_message_to_log_file(log_file, node_footer, node_footer_size);
		}
		char node_footer[6] = "</li>";
		size_t node_footer_size = 5;
		profiler_write_message_to_log_file(log_file, node_footer, node_footer_size);
	} else if (export_type == TEXT) {
		return;
	}
}

void profiler_exporter_before_messages(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char title[51] = "<div class=\"title\">Messages</div><ul class=\"list\">";
		size_t title_size = 50;
		profiler_write_message_to_log_file(log_file, title, title_size);
	} else if (export_type == TEXT) {
		char title[11] = "\nMessages\n";
		size_t title_size = 10;
		profiler_write_message_to_log_file(log_file, title, title_size);
	}
}

void profiler_exporter_after_messages(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char close_tag[6] = "</ul>";
		size_t close_tag_size = 5;
		profiler_write_message_to_log_file(log_file, close_tag, close_tag_size);
	}
}

static void profiler_system_time_to_string(char *string, size_t string_size, INT64 *system_time) {
	TIME_FIELDS ts;
	RtlTimeToTimeFields((PLARGE_INTEGER)system_time, &ts);
	RtlStringCbPrintfA(string, string_size, "%04d.%02d.%02d %02d:%02d:%02d.%03d%04d", ts.Year, ts.Month, ts.Day, ts.Hour, ts.Minute, ts.Second, ts.Milliseconds, (*system_time) % 10000);
}

void profiler_exporter_message_header(HANDLE log_file, export_type_t export_type, message_list_node_t *message, int number) {
	if (export_type == HTML) {
		char message_buffer[PROFILER_MESSAGE_BUFFER_SIZE];
		size_t message_buffer_size = PROFILER_MESSAGE_BUFFER_SIZE;
		size_t message_size;

		char *open_tag = "<li><div class=\"tree-node\"><div class=\"tree-node-timestamp\">";
		size_t open_tag_size = 60;
		profiler_write_message_to_log_file(log_file, open_tag, open_tag_size);

		char timestamp[28];
		size_t timestamp_size = 28;
		profiler_system_time_to_string(timestamp, timestamp_size, &message->timestamp);
		timestamp_size -= 1;
		profiler_write_message_to_log_file(log_file, timestamp, timestamp_size);

		char *ts_close_tag = "</div> ";
		size_t ts_close_tag_size = 7;
		profiler_write_message_to_log_file(log_file, ts_close_tag, ts_close_tag_size);


		/*insert all info in tags*/
		switch (message->type) {
		case TIMESTAMP:
			RtlStringCbPrintfA(message_buffer, message_buffer_size, "%s", message->message);
			break;
		case TOTAL_TIME:
		case TOTAL_WAIT_TIME:
		case TIME_INTERVAL:
			RtlStringCbPrintfA(message_buffer, message_buffer_size, "%s:<div class=\"tree-node-time\">%lld&micro;s</div>", message->message, message->time);
			break;
		case THREAD_WAIT_TIME_INTERVAL:
		case THREAD_CPU_TIME_INTERVAL:
		case THREAD_TOTAL_WAIT_TIME:
		case THREAD_TOTAL_CPU_TIME:
		case THREAD_TOTAL_TIME:
		default:
			RtlStringCbPrintfA(message_buffer, message_buffer_size, "<div class=\"tree-node-thread\">[%p]</div> %s:<div class=\"tree-node-time\">%lld&micro;s</div>", message->thread_id, message->message, message->time);
			break;
		}
		RtlStringCbLengthA(message_buffer, message_buffer_size, &message_size);
		profiler_write_message_to_log_file(log_file, message_buffer, message_size);

		char *close_tag = "</div></li>";
		size_t close_tag_size = 11;
		profiler_write_message_to_log_file(log_file, close_tag, close_tag_size);
	} else if (export_type == TEXT) {
		char message_buffer[PROFILER_MESSAGE_BUFFER_SIZE];
		size_t message_buffer_size = PROFILER_MESSAGE_BUFFER_SIZE;
		size_t message_size;

		char timestamp[28];
		size_t timestamp_size = 28;
		profiler_system_time_to_string(timestamp, timestamp_size, &message->timestamp);

		switch (message->type) {
		case TIMESTAMP:
			RtlStringCbPrintfA(message_buffer, message_buffer_size, "%u %s %s\n", number, timestamp, message->message);
			break;
		case TOTAL_TIME:
		case TOTAL_WAIT_TIME:
		case TIME_INTERVAL:
			RtlStringCbPrintfA(message_buffer, message_buffer_size, "%u %s %s: %lldus\n", number, timestamp, message->message, message->time);
			break;
		case THREAD_WAIT_TIME_INTERVAL:
		case THREAD_CPU_TIME_INTERVAL:
		case THREAD_TOTAL_WAIT_TIME:
		case THREAD_TOTAL_CPU_TIME:
		case THREAD_TOTAL_TIME:
		default:
			RtlStringCbPrintfA(message_buffer, message_buffer_size, "%u %s [%p] %s: %lldus\n", number, timestamp, message->thread_id, message->message, message->time);
			break;
		}
		RtlStringCbLengthA(message_buffer, message_buffer_size, &message_size);
		profiler_write_message_to_log_file(log_file, message_buffer, message_size);
	}
}

void profiler_exporter_message_footer(HANDLE log_file, export_type_t export_type, message_list_node_t *message, int number) {
	UNREFERENCED_PARAMETER(log_file);
	UNREFERENCED_PARAMETER(export_type);
	UNREFERENCED_PARAMETER(message);
	UNREFERENCED_PARAMETER(number);
}

void profiler_exporter_footer(HANDLE log_file, export_type_t export_type) {
	if (export_type == HTML) {
		char footer[15] = "</body></html>";
		size_t footer_size = 14;
		profiler_write_message_to_log_file(log_file, footer, footer_size);
	} else if (export_type == TEXT) {
		return;
	}
}
