#ifndef PROFILER_H
#define PROFILER_H

typedef struct time_pair_t {
	INT64 t1;
	INT64 t2;
} time_pair_t;

HANDLE profiler_create_log_file(const wchar_t *filename);

void profiler_close_log_file(HANDLE file_handle);

void profiler_write_message_to_log_file(HANDLE file, char *message, size_t size);

void profiler_initialize(void);

void profiler_deinitialize(void);

void profiler_total_time(void);

void profiler_total_wait_time(void);

void profiler_timestamp(void);

INT64 profiler_time_begin(void);

void profiler_time_end(INT64 *begin_time);

void profiler_thread_initialize(void);

void profiler_thread_all_time_begin(INT64 *cpu_time, INT64 *real_time);

void profiler_thread_all_time_end(INT64 *cpu_time, INT64 *real_time);

time_pair_t profiler_thread_wait_time_begin();

void profiler_thread_wait_time_end(time_pair_t *begin_time);

INT64 profiler_thread_cpu_time_begin(void);

void profiler_thread_cpu_time_end(INT64 *begin_time);
	
void profiler_thread_total_wait_time(void);

void profiler_thread_total_cpu_time(void);

void profiler_thread_total_time(void);

void profiler_export_messages(HANDLE log_file);

#endif /* PROFILER_H */
