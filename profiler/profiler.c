#include <ntddk.h>
#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler.h"
#include "profiler_class_c.h"
#include "profiler_message_list.h"


#define PROFILER_THREAD_START_TIME_TAG 'ptst'

#define PROFILER_FILENAME_BUFFER 'pflb'

#define PROFILER_MESSAGE_BUFFER_SIZE 1024


typedef struct thread_start_time_t {
	HANDLE thread_id;
	PKTHREAD thread_handle;
	LARGE_INTEGER start_time;
	struct thread_start_time_t *next;
} thread_start_time_t;


static LARGE_INTEGER start_process_time;

static thread_start_time_t *start_time_list = NULL;


static void profiler_add_thread_start_time(HANDLE thread_id, PKTHREAD thread_handle, LARGE_INTEGER start_time) {
	thread_start_time_t *thread_time = (thread_start_time_t*)ExAllocatePoolWithTag(PagedPool, sizeof(thread_start_time_t), PROFILER_THREAD_START_TIME_TAG);
	thread_time->thread_id = thread_id;
	thread_time->thread_handle = thread_handle;
	thread_time->start_time = start_time;
	thread_time->next = NULL;
	if (start_time_list == NULL) {
		start_time_list = thread_time;
	} else {
		thread_start_time_t *element = start_time_list;
		while (element->next != NULL) {
			element = element->next;
		}
		element->next = thread_time;
	}
}

static PLARGE_INTEGER profiler_get_thread_start_time(HANDLE thread_id) {
	PLARGE_INTEGER start_time = NULL;
	thread_start_time_t *element = start_time_list;
	while (element != NULL) {
		if (element->thread_id == thread_id) {
			start_time = &element->start_time;
			break;
		}
		element = element->next;
	}
	return start_time;
}

HANDLE profiler_create_log_file(const wchar_t *filename) {
	HANDLE file_handle;
	IO_STATUS_BLOCK status_block;
	NTSTATUS status;
	UNICODE_STRING file_name;
	OBJECT_ATTRIBUTES file_attributes;
	LARGE_INTEGER timestamp;
	TIME_FIELDS ts;

	const wchar_t *filename_prefix = L"\\DosDevices\\C:\\";
	size_t fp_size;
	RtlStringCbLengthW(filename_prefix, 1024, &fp_size);
	KeQuerySystemTime(&timestamp);
	RtlTimeToTimeFields(&timestamp, &ts);
	wchar_t timestamp_prefix[20];
	size_t tsp_size = 20 * sizeof(wchar_t);
	RtlStringCbPrintfW(timestamp_prefix, tsp_size, L"%04d-%02d-%02dT%02d.%02d.%02d", ts.Year, ts.Month, ts.Day, ts.Hour, ts.Minute, ts.Second);
	tsp_size -= sizeof(wchar_t);
	size_t f_size;
	RtlStringCbLengthW(filename, 1024, &f_size);
	size_t f_length = f_size / sizeof(wchar_t);
	size_t del_pos = 0;
	size_t i;
	for (i = 0; i <= f_length; ++i) {
		if (filename[i] == '\\') {
			del_pos = i + 1;
		}
	}
	size_t fbuffer_size = fp_size + tsp_size + f_size + sizeof(wchar_t);
	wchar_t *filename_buffer = (wchar_t*)ExAllocatePoolWithTag(PagedPool, fbuffer_size, PROFILER_FILENAME_BUFFER);
	RtlInitEmptyUnicodeString(&file_name, filename_buffer, (USHORT)fbuffer_size);
	RtlUnicodeStringCatString(&file_name, filename_prefix);
	RtlUnicodeStringCchCatStringN(&file_name, filename, del_pos);
	RtlUnicodeStringCatString(&file_name, timestamp_prefix);
	RtlAppendUnicodeToString(&file_name, filename + del_pos);

	InitializeObjectAttributes(&file_attributes, &file_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL);
	ACCESS_MASK file_access_mask = STANDARD_RIGHTS_WRITE | FILE_WRITE_DATA;
	status = ZwCreateFile(&file_handle, 
		file_access_mask, 
		&file_attributes, 
		&status_block, 
		NULL, 
		FILE_ATTRIBUTE_NORMAL, 
		0, 
		FILE_OVERWRITE_IF,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 
		0);
	if (status != STATUS_SUCCESS) {
		/* bad */
	}
	return file_handle;
}

void profiler_close_log_file(HANDLE file) {
	ZwClose(file);
}

void profiler_write_message_to_log_file(HANDLE log_file, char *message, size_t size) {
	NTSTATUS status;
	IO_STATUS_BLOCK status_block;
	PVOID buffer = (PVOID)message;
	ULONG length = (ULONG)size;
	status = ZwWriteFile(log_file, NULL, NULL, NULL, &status_block, buffer, length, NULL, NULL);
	if (!NT_SUCCESS(status)) {
		/* bad */
	}
}

void profiler_initialize(void) {
	start_process_time = KeQueryPerformanceCounter(NULL);
	profiler_thread_initialize();
}

void profiler_deinitialize(void) {
	thread_start_time_t *next_element = start_time_list;
	thread_start_time_t	*current_element;
	while (next_element != NULL) {
		current_element = next_element;
		next_element = current_element->next;
		ExFreePoolWithTag(current_element, PROFILER_THREAD_START_TIME_TAG);
	}
}

void profiler_thread_initialize(void) {
	HANDLE thread_id = PsGetCurrentThreadId();
	PKTHREAD thread_handle = KeGetCurrentThread();
	LARGE_INTEGER start_time = KeQueryPerformanceCounter(NULL);
	profiler_add_thread_start_time(thread_id, thread_handle, start_time);
}

void profiler_total_time(void) {
	LARGE_INTEGER frequency;
	LARGE_INTEGER end_process_time = KeQueryPerformanceCounter(&frequency);
	/* convert to us */
	INT64 total_time = (end_process_time.QuadPart - start_process_time.QuadPart) * 1000000;
	total_time /= frequency.QuadPart;

	profiler_c_add_message(TOTAL_TIME, "Total time", total_time, NULL);
}

void profiler_total_wait_time(void) {
	INT64 total_wait_time = 0;
	thread_start_time_t *current_element = start_time_list;
	while (current_element != NULL) {
		ULONG user_time;
		ULONG kernel_time = KeQueryRuntimeThread(current_element->thread_handle, &user_time);
		ULONG clock_increment = KeQueryTimeIncrement();
		INT64 cpu_time = (kernel_time + user_time) * clock_increment;
		LARGE_INTEGER frequency;
		LARGE_INTEGER end_real_time = KeQueryPerformanceCounter(&frequency);
		/* convert to 100 ns */
		INT64 real_time = (end_real_time.QuadPart - current_element->start_time.QuadPart) * 10000000;
		real_time /= frequency.QuadPart;
		INT64 wait_time = real_time - cpu_time;
		total_wait_time += wait_time;
		current_element = current_element->next;
	}
	/* convert to us */
	total_wait_time /= 10;

	profiler_c_add_message(TOTAL_WAIT_TIME, "Total wait time", total_wait_time, NULL);
}

void profiler_timestamp(void) {
	profiler_c_add_message(TIMESTAMP, "Timestamp", 0, NULL);
}

INT64 profiler_time_begin(void) {
	LARGE_INTEGER begin_time = KeQueryPerformanceCounter(NULL);
	return (INT64)begin_time.QuadPart;
}

void profiler_time_end(INT64 *begin_time) {
	LARGE_INTEGER frequency;
	LARGE_INTEGER end_time = KeQueryPerformanceCounter(&frequency);
	/* convert to us */
	INT64 real_time = (end_time.QuadPart - *begin_time) * 1000000;
	real_time /= frequency.QuadPart;

	profiler_c_add_message(TIME_INTERVAL, "Time interval", real_time, NULL);
}

void profiler_thread_all_time_begin(INT64 *cpu_time, INT64 *real_time) {
	*real_time = (INT64)KeQueryPerformanceCounter(NULL).QuadPart;
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	*cpu_time = user_time + kernel_time;
}

void profiler_thread_all_time_end(INT64 *cpu_time, INT64 *real_time) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 end_cpu_time = user_time + kernel_time;
	LARGE_INTEGER frequency;
	LARGE_INTEGER end_real_time = KeQueryPerformanceCounter(&frequency);
	*real_time = (end_real_time.QuadPart - *real_time) * 1000000;
	*real_time /= frequency.QuadPart;
	*cpu_time = (end_cpu_time - *cpu_time) * clock_increment / 10;
}

time_pair_t profiler_thread_wait_time_begin() {
	time_pair_t begin_time;
	begin_time.t1 = (INT64)KeQueryPerformanceCounter(NULL).QuadPart;
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	begin_time.t2 = (user_time + kernel_time) * clock_increment;
	return begin_time;
}

void profiler_thread_wait_time_end(time_pair_t *begin_time) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 end_cpu_time = (user_time + kernel_time) * clock_increment;
	LARGE_INTEGER frequency;
	LARGE_INTEGER end_real_time = KeQueryPerformanceCounter(&frequency);
	/* convert to 100 ns */
	INT64 real_time = (end_real_time.QuadPart - begin_time->t1) * 10000000;
	real_time /= frequency.QuadPart;
	INT64 cpu_time = end_cpu_time - begin_time->t2;
	/* convert to us */
	INT64 wait_time = (real_time - cpu_time) / 10;

	profiler_c_add_message(THREAD_WAIT_TIME_INTERVAL, "Thread wait time interval", wait_time, PsGetCurrentThreadId());
}

INT64 profiler_thread_cpu_time_begin(void) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 begin_cpu_time = (user_time + kernel_time) * clock_increment;
	return begin_cpu_time;
}

void profiler_thread_cpu_time_end(INT64 *begin_cpu_time) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 end_cpu_time = (user_time + kernel_time) * clock_increment;
	/* convert to us */
	INT64 cpu_time = (end_cpu_time - *begin_cpu_time) / 10;

	profiler_c_add_message(THREAD_CPU_TIME_INTERVAL, "Thread CPU time interval", cpu_time, PsGetCurrentThreadId());
}

void profiler_thread_total_wait_time(void) {
	PKTHREAD current_thread = KeGetCurrentThread();
	HANDLE thread_id = PsGetCurrentThreadId();
	PLARGE_INTEGER start_real_time = profiler_get_thread_start_time(thread_id);
	if (start_real_time == NULL) {
		start_real_time = &start_process_time;
	}

	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 cpu_time = (user_time + kernel_time) * clock_increment;
	LARGE_INTEGER frequency;
	LARGE_INTEGER end_real_time = KeQueryPerformanceCounter(&frequency);
	/* convert to 100 ns */
	INT64 real_time = (end_real_time.QuadPart - start_real_time->QuadPart) * 10000000;
	real_time /= frequency.QuadPart;
	/* convert to us */
	INT64 thread_wait_time = (real_time - cpu_time) / 10;

	profiler_c_add_message(THREAD_TOTAL_WAIT_TIME, "Thread total wait time", thread_wait_time, thread_id);
}

void profiler_thread_total_cpu_time(void) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 cpu_time = (user_time + kernel_time) * clock_increment;
	/* convert to us */
	cpu_time /= 10;

	profiler_c_add_message(THREAD_TOTAL_CPU_TIME, "Thread total CPU time", cpu_time, PsGetCurrentThreadId());
}

void profiler_thread_total_time(void) {
	HANDLE thread_id = PsGetCurrentThreadId();
	PLARGE_INTEGER start_real_time = profiler_get_thread_start_time(thread_id);
	if (start_real_time == NULL) {
		start_real_time = &start_process_time;
	}
	LARGE_INTEGER frequency;
	LARGE_INTEGER end_real_time = KeQueryPerformanceCounter(&frequency);
	/* convert to us */
	INT64 total_time = (end_real_time.QuadPart - start_real_time->QuadPart) * 1000000;
	total_time /= frequency.QuadPart;

	profiler_c_add_message(THREAD_TOTAL_TIME, "Thread total time", total_time, thread_id);
}
