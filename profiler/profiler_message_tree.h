#ifndef PROFILER_MESSAGE_TREE_H
#define PROFILER_MESSAGE_TREE_H

#include <ntdef.h>

struct message_tree_node_t;

typedef struct message_tree_child_t {
	struct message_tree_node_t *child;
	struct message_tree_child_t *next;
} message_tree_child_t;

typedef struct message_tree_node_t {
	struct message_tree_node_t *parent;
	message_tree_child_t *children;
	message_tree_child_t *last_child;
	
	const char *file;
	const char *message;
	INT64 cpu_time;
	INT64 real_time;
	int line;
} message_tree_node_t;

typedef struct message_tree_t {
	message_tree_node_t *head;
	message_tree_node_t *last;
} message_tree_t;


void profiler_message_tree_initialize(message_tree_t *tree);

void profiler_message_tree_deinitialize(message_tree_t *tree);

message_tree_node_t* profiler_message_tree_add(message_tree_t *tree, const char *note);

void profiler_message_tree_return(message_tree_t *tree, message_tree_node_t *node);


#endif /* PROFILER_MESSAGE_TREE_H */