#include <ntddk.h>
#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler_message_tree.h"


#define PROFILER_MESSAGE_TREE_TAG 'pmtt'

#define PROFILER_MESSAGE_TREE_NODE_TAG 'pmnt'

#define PROFILER_MESSAGE_TREE_CHILDREN_TAG 'pmct'

#define PROFILER_MAX_NOTE_SIZE 256


static void profiler_message_tree_node_add_child(message_tree_node_t *node, message_tree_node_t *child_node) {
	message_tree_child_t *new_child = (message_tree_child_t*)ExAllocatePoolWithTag(PagedPool, sizeof(message_tree_child_t), PROFILER_MESSAGE_TREE_CHILDREN_TAG);
	new_child->child = child_node;
	new_child->next = NULL;
	if (node->children == NULL) {
		node->children = new_child;
		node->last_child = new_child;
	} else {
		node->last_child->next = new_child;
		node->last_child = new_child;
	}
}

void profiler_message_tree_initialize(message_tree_t *tree) {
	message_tree_node_t *head = (message_tree_node_t*)ExAllocatePoolWithTag(PagedPool, sizeof(message_tree_node_t), PROFILER_MESSAGE_TREE_NODE_TAG);
	head->parent = NULL;
	head->line = 0;
	head->file = "";
	head->message = (const char*)PsGetCurrentThreadId();
	head->children = NULL;
	head->last_child = NULL;
	tree->head = head;
	tree->last = head;
}

static void profiler_message_tree_node_deinitialize(message_tree_node_t *node) {
	message_tree_child_t *children = node->children;
	message_tree_child_t *prev;
	while (children != NULL) {
		profiler_message_tree_node_deinitialize(children->child);
		prev = children;
		children = children->next;
		ExFreePoolWithTag(prev, PROFILER_MESSAGE_TREE_CHILDREN_TAG);
	}

	ExFreePoolWithTag(node, PROFILER_MESSAGE_TREE_NODE_TAG);
}

void profiler_message_tree_deinitialize(message_tree_t *tree) {
	profiler_message_tree_node_deinitialize(tree->head);
}

message_tree_node_t* profiler_message_tree_add(message_tree_t *tree, const char *note) {
	size_t note_size;
	RtlStringCbLengthA(note, PROFILER_MAX_NOTE_SIZE, &note_size);
	note_size += 1;
	size_t node_size = sizeof(message_tree_node_t) + note_size;
	message_tree_node_t *node = (message_tree_node_t*)ExAllocatePoolWithTag(PagedPool, node_size, PROFILER_MESSAGE_TREE_NODE_TAG);
	char *note_copy = (char*)node + sizeof(message_tree_node_t);
	RtlStringCbCopyA(note_copy, note_size, note);
	node->message = note_copy;

	node->parent = tree->last;
	node->children = NULL;
	node->last_child = NULL;
	profiler_message_tree_node_add_child(tree->last, node);
	tree->last = node;
	return node;
}

void profiler_message_tree_return(message_tree_t *tree, message_tree_node_t *node) {
	tree->last = node->parent;
}
