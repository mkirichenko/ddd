#ifndef PROFLILER_EXPORTER_H
#define PROFLILER_EXPORTER_H

#include "profiler_message_tree.h"
#include "profiler_message_list.h"

typedef enum export_type_t {
	TEXT,
	HTML
} export_type_t;

void profiler_exporter_header(HANDLE log_file, export_type_t export_type);

void profiler_exporter_before_thread_trees(HANDLE log_file, export_type_t export_type);

void profiler_exporter_after_thread_trees(HANDLE log_file, export_type_t export_type);

void profiler_exporter_thread_tree_header(HANDLE log_file, export_type_t export_type);

void profiler_exporter_thread_tree_footer(HANDLE log_file, export_type_t export_type);

void profiler_exporter_thread_tree_node_header(HANDLE log_file, export_type_t export_type, message_tree_node_t *node, int level);

void profiler_exporter_thread_tree_node_footer(HANDLE log_file, export_type_t export_type, message_tree_node_t *node, int level);

void profiler_exporter_before_messages(HANDLE log_file, export_type_t export_type);

void profiler_exporter_after_messages(HANDLE log_file, export_type_t export_type);

void profiler_exporter_message_header(HANDLE log_file, export_type_t export_type, message_list_node_t *message, int number);

void profiler_exporter_message_footer(HANDLE log_file, export_type_t export_type, message_list_node_t *message, int number);

void profiler_exporter_footer(HANDLE log_file, export_type_t export_type);

#endif /* PROFLILER_EXPORTER_H */

