/*++

Module Name:

    public.h

Abstract:

    This module contains the common declarations shared by driver
    and user applications.

Environment:

    user and kernel

--*/

//
// Define an Interface Guid so that apps can find the device and talk to it.
//

DEFINE_GUID (GUID_DEVINTERFACE_KMDFAsync2,
    0x6a5c5218,0xa2a6,0x49a2,0xa0,0xfc,0x05,0x46,0x90,0xf7,0x96,0x73);
// {6a5c5218-a2a6-49a2-a0fc-054690f79673}
