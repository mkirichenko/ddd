#ifndef ASYNC_TYPES_H
#define ASYNC_TYPES_H

#include <ntdef.h>

typedef struct context_registers_t {
	/* nonvolatile GPR */
	UINT64 rbx;
	UINT64 rsi;
	UINT64 rdi;
	UINT64 rbp;
	UINT64 rsp;
	UINT64 r12;
	UINT64 r13;
	UINT64 r14;
	UINT64 r15;
} context_registers_t;

typedef struct context_t {
	void *stack;
	void *ret_address;
	UINT64 state;
	context_registers_t registers;
} context_t;

typedef void(*coroutine_body_t)(void*);

typedef struct queue_element_t {
	coroutine_body_t body;
	void *argument;
	context_t *context;
	struct queue_element_t *next;
	struct queue_element_t *prev;
} queue_element_t;

typedef struct queue_t {
	queue_element_t *head;
} queue_t;

#endif /* ASYNC_TYPES_H */

