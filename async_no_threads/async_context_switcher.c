#include <wdm.h>
#include "async_context_switcher.h"
#include "async_queue.h"


#define ASYNC_CONTEXT_STACK_TAG 'acs0'

static void *async_context_allocate_stack() {
	/* 4Kb */
	return ExAllocatePoolWithTag(PagedPool, 0x1000, ASYNC_CONTEXT_STACK_TAG);
}

static void async_context_deallocate_stack(void *stack) {
	ExFreePoolWithTag(stack, ASYNC_CONTEXT_STACK_TAG);
}

void async_initial_context_switch(context_t *from_context, queue_element_t *queue_element) {
	from_context->state = CONTEXT_STATE_WAITING;
	void *to_stack = async_context_allocate_stack();
	async_initial_context_switch_asm(from_context, queue_element, to_stack);
}

void async_context_switch(context_t *from_context, context_t *to_context) {
	if (to_context->state == CONTEXT_STATE_WAITING) {
		from_context->state = CONTEXT_STATE_WAITING;
		to_context->state = CONTEXT_STATE_RUNNING;
		async_context_switch_asm(from_context, to_context);
	} else {
		/* wrong state */
	}
}

void async_final_context_switch(context_t *from_context, context_t *to_context) {
	if (to_context->state == CONTEXT_STATE_WAITING) {
		to_context->state = CONTEXT_STATE_RUNNING;
		from_context->state = CONTEXT_STATE_ENDED;
		async_final_context_switch_asm(to_context);
	} else {
		/* wrong state */
	}
}


void async_final_initial_context_switch(context_t *from_context, queue_element_t *queue_element) {
	void *to_stack = async_context_allocate_stack();
	from_context->state = CONTEXT_STATE_ENDED;
	async_final_initial_context_switch_asm(queue_element, to_stack);
}

void async_context_prepare_initial(context_t *context) {
	context->stack = NULL;
	context->state = CONTEXT_STATE_RUNNING;
}

void async_finalize_context(context_t *context) {
	if (context->stack != NULL) {
		ExFreePoolWithTag(context->stack, ASYNC_CONTEXT_STACK_TAG);
	}
}
