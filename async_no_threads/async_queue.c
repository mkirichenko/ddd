#include <wdm.h>
#include "async_queue.h"


#define QUEUE_PUT_TAG 'qpt0'


void queue_initialize(queue_t *queue) {
	queue->head = NULL;
}

int queue_is_empty(queue_t *queue) {
	if (queue->head == NULL) {
		return 1;
	} else {
		return 0;
	}
}

void queue_put_first(queue_t *queue, coroutine_body_t body, void *argument) {
	queue_element_t *element = (queue_element_t*)ExAllocatePoolWithTag(PagedPool, sizeof(queue_element_t), QUEUE_PUT_TAG);	
	element->body = body;
	element->argument = argument;
	element->context = NULL;
	if (queue->head == NULL) {
		element->next = NULL;
		element->prev = NULL;
		queue->head = element;
	} else {
		queue_element_t *head = queue->head;
		element->next = head;
		if (head->prev == NULL) {
			head->next = element;
			element->prev = head;
		} else {
			head->prev->next = element;
			element->prev = head->prev;
		}
		head->prev = element;
		queue->head = element;
	}
}

void queue_put(queue_t *queue, coroutine_body_t body, void *argument) {
	queue_element_t *element = (queue_element_t*)ExAllocatePoolWithTag(PagedPool, sizeof(queue_element_t), QUEUE_PUT_TAG);
	element->body = body;
	element->argument = argument;
	element->context = NULL;
	if (queue->head == NULL) {
		element->next = NULL;
		element->prev = NULL;
		queue->head = element;
	} else {
		queue_element_t *head = queue->head;
		element->next = head;
		if (head->prev == NULL) {
			head->next = element;
			element->prev = head;
		} else {
			head->prev->next = element;
			element->prev = head->prev;
		}
		head->prev = element;
	}
}

queue_element_t *queue_get(queue_t *queue) {
 	queue_element_t *head = queue->head;
	if (queue->head->next != NULL) {
		queue->head = queue->head->next;
	}
	return head;
}

void queue_release_element(queue_element_t *element) {
	ExFreePoolWithTag(element, QUEUE_PUT_TAG);
}

void queue_remove(queue_t *queue, queue_element_t *element) {
	if (queue->head == element) {
		if (queue->head->next == NULL) {
			queue->head = NULL;			
		} else {
			queue_element_t *new_head = queue->head->next;
			if (queue->head->next == queue->head->prev) {
				new_head->next = NULL;
				new_head->prev = NULL;
			} else {
				new_head->prev = queue->head->prev;
				queue->head->prev->next = new_head;				
			}
			queue->head = new_head;
		}
	} else {
		queue_element_t *prev = element->prev;
		queue_element_t *next = element->next;
		if (prev == next) { /* == queue->head */
			queue->head->next = NULL;
			queue->head->prev = NULL;
		} else {
			prev->next = next;
			next->prev = prev;
		}
	}
	ExFreePoolWithTag(element, QUEUE_PUT_TAG);
}

void queue_deinitialize(queue_t *queue) {
	queue_element_t *current = queue->head;
	queue_element_t *next;
	while (current != NULL) {
		next = current->next;
		ExFreePoolWithTag(current, QUEUE_PUT_TAG);
		current = next;
	}
	queue->head = NULL;
}
