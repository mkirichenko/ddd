#include <wdm.h>
#include "context_switcher.h"

#define CONTEXT_STACK_TAG 'cxst'

context_t* context_switch(context_t *from_context, coroutine_body_t to_function, void *to_arg);
	from_context->state = CONTEXT_STATE_WAITING;
	void *to_stack = context_allocate_stack();
	return context_switch_asm(from_context, to_function, to_arg, to_stack);
}

int context_yield(context_t *from_context, context_t *to_context) {
	if (to_context->state == CONTEXT_STATE_ENDED) {
		void *stack = to_context->stack;
		context_deallocate_stack(stack);
		return CONTEXT_STATE_INVALID;
	}
	if (to_context->state == CONTEXT_STATE_WAITING) {
		from_context->state = CONTEXT_STATE_WAITING;
		to_context->state = CONTEXT_STATE_RUNNING;
		context_yield_asm(from_context, to_context);
		return (int)to_context->state;
	} else {
		/* panic */
	}
}

void context_return(context_t *from_context, context_t *to_context) {
	if (to_context->state == CONTEXT_STATE_WAITING) {
		from_context->state = CONTEXT_STATE_ENDED;
		to_context->state = CONTEXT_STATE_RUNNING;
		context_yield_asm(from_context, to_context);
	} else {
		/* panic */
	}
}

void context_prepare_initial(context_t *context) {
	context->stack = NULL;
	context->state = CONTEXT_STATE_RUNNING;
}

static void* context_allocate_stack() {
	/* 4Kb */
	size_t stack_size = 0x1000;
	void *stack = ExAllocatePoolWithTag(PagedPool, stack_size, CONTEXT_STACK_TAG);
	return stack;
}

static void context_deallocate_stack(void *stack) {
	/* 4Kb */
	size_t stack_size = 0x1000;
	ExFreePoolWithTag(stack, CONTEXT_STACK_TAG);
}