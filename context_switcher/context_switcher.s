; context_t
; size:
;   0x60 (96) bytes
; 	:
;   stack - 0x0
;   ret_address - 0x8
;   state - 0x10
;   rbx - 0x18
;   rsi - 0x20
;   rdi - 0x28
;   rbp - 0x30
;   rsp - 0x38
;   r12 - 0x40
;   r13 - 0x48
;   r14 - 0x50
;   r15 - 0x58




.CODE

context_switch_asm PROC
	.ENDPROLOG
	; arguments:	
	; rcx - from_context
	; rdx - to_function
	; r8 - to_arg
	; r9 - to_stack
	
	; saving registers
	mov [rcx+018h], rbx
	mov [rcx+020h], rsi
	mov [rcx+028h], rdi
	mov [rcx+030h], rbp
	mov [rcx+038h], rsp
	mov [rcx+040h], r12
	mov [rcx+048h], r13
	mov [rcx+050h], r14
	mov [rcx+058h], r15
		
;	; saving stack limits
;	mov eax, fs:[04h]
;	mov [rcx+060h], eax
;	mov eax, fs:[08h]
;	mov [rcx+064h], eax
	
	; saving return address in from_context
	lea [rcx+08h], returning
	
	; creating to_context
	mov r12, r9
	; saving stack pointer in to_context
	mov [r12+0fa0h], r9

	
;	; loading new stack limits
;	lea eax, [r9+01000h]
;	mov fs:[04h], eax
;	mov [r12+060h], eax
;	lea eax, [r9+068h]
;	mov fs:[08h], eax
;	mov [r12+064h], eax
	
	; loading new stack pointers
	lea rsp, [r9+0fa0h]
	lea rbp, [r9+0fa0h]
		
	; preparing arguments for to_function
	; rcx - same
	mov r11, rdx
	mov rdx, r12
	; r8 - same	

	; jumping in to_function
	jmp r11

returning:
	; preparing return value
	mov rax r12
	ret
	
context_switch_asm ENDP





context_yield_asm PROC
	.ENDPROLOG
	; arguments:	
	; rcx - from_context
	; rdx - to_context
	
	; saving registers
	mov [rcx+018h], rbx
	mov [rcx+020h], rsi
	mov [rcx+028h], rdi
	mov [rcx+030h], rbp
	mov [rcx+038h], rsp
	mov [rcx+040h], r12
	mov [rcx+048h], r13
	mov [rcx+050h], r14
	mov [rcx+058h], r15
;	; saving stack limits
;	mov eax, fs:[04h]
;	mov [rcx+060h], eax
;	mov eax, fs:[08h]
;	mov [rcx+064h], eax
	
	;saving return address in from_context
	lea [rcx+08h], returning
	
	;loading registers
	mov rbx, [rdx+018h]
	mov rsi, [rdx+020h]
	mov rdi, [rdx+028h]
	mov rbp, [rdx+030h]
	mov rsp, [rdx+038h]
	mov r12, [rdx+040h]
	mov r13, [rdx+048h]
	mov r14, [rdx+050h]
	mov r15, [rdx+058h]
;	; loading stack limits
;	mov eax, [rdx+060h]
;	mov fs:[04h], eax
;	mov eax, [rdx+064h]
;	mov fs:[08h], eax
	
	; preparing arguments for context_switch_asm returning
	mov r12, rcx
	
	; jumping to return address in to_context
	mov r11, [rdx+08h]
	jmp r11
	
returning:
	ret
	
context_yield_asm ENDP

END
