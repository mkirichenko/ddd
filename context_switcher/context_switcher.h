#include <stdint.h>

#define CONTEXT_STATE_RUNNING 0x0
#define CONTEXT_STATE_WAITING 0x1
#define CONTEXT_STATE_ENDED 0x2
#define CONTEXT_STATE_INVALID 0x3

typedef void (*coroutine_body_t)(context_t*, context_t*, void*);

typedef struct context_registers_t {
	/* nonvolatile GPR */
	uint64_t rbx;
	uint64_t rsi;
	uint64_t rdi;
	uint64_t rbp;
	uint64_t rsp;
	uint64_t r12;
	uint64_t r13;
	uint64_t r14;
	uint64_t r15;
} context_registers_t;

typedef struct context_t {
	void *stack;
	void *ret_address;
	uint64_t state;
	context_registers_t registers;
} context_t;

context_t* context_switch(context_t *from_context, coroutine_body_t to_function, void *to_arg);

int context_yield(context_t *from_context, context_t *to_context);

void context_return(context_t *from_context, context_t *to_context);

void context_prepare_initial(context_t *context);
