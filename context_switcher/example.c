#include "context_switcher.h"

void other_fuction(context_t *from_context, context_t *to_context, void *arg) {
	int a, b;
	
	a = 0;
	b = 1;
	b = a + b;
	
	context_yield(from_context, to_context);
	
	a = a + b;
	
	context_return(from_context, to_context);
}

int main(int argc, void **argv) {
	int a, b, e;
	context_t from_context;
	context_t *to_context;
	
	context_prepare_initial(&from_context);
	to_context = context_switch(&from_context, &other_function, NULL);
	
	a = 1;
	b = 2;
	b = a + b; 
	
	context_yield(&from_context, to_context);
	
	a = a + b;
	
	e = context_yield(&from_context, to_context);
	if (e == CONTEXT_STATE_INVALID) {
		to_context = NULL;
	}
	
	b = a + b;
	
	return 0;
}
