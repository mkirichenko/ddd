#ifndef ASYNC_CONTEXT_SWITCHER_H
#define ASYNC_CONTEXT_SWITCHER_H

#include "async_types.h"

#define CONTEXT_STATE_RUNNING 0x0
#define CONTEXT_STATE_WAITING 0x1
#define CONTEXT_STATE_ENDED 0x2
#define CONTEXT_STATE_INVALID 0x3

extern void async_initial_context_switch_asm(context_t *from_context, queue_element_t *queue_element, void *to_stack);

extern void async_context_switch_asm(context_t *from_context, context_t *to_context);

extern void async_final_context_switch_asm(context_t *to_context);

extern void async_final_initial_context_switch_asm(queue_element_t *queue_element, void *to_stack);

void async_initial_context_switch(context_t *from_context, queue_element_t *queue_element);

void async_context_switch(context_t *from_context, context_t *to_context);

void async_final_context_switch(context_t *from_context, context_t *to_context);

void async_final_initial_context_switch(context_t *from_context, queue_element_t *queue_element);

void async_context_prepare_initial(context_t *context);

void async_finalize_context(context_t *context);

#endif /* ASYNC_CONTEXT_SWITCHER_H */
