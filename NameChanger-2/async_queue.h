#ifndef ASYNC_QUEUE_H
#define ASYNC_QUEUE_H
#include "async_types.h"

void queue_initialize(queue_t *queue);

int queue_is_empty(queue_t *queue);

void queue_put_first(queue_t *queue, coroutine_body_t body, void *argument);

void queue_put(queue_t *queue, coroutine_body_t body, void *argument);

queue_element_t *queue_get(queue_t *queue);

void queue_release_element(queue_element_t *element);

void queue_remove(queue_t *queue, queue_element_t *element);

void queue_deinitialize(queue_t *queue);

#endif /* ASYNC_QUEUE_H */
