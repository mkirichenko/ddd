#include <wdm.h>
#include "async_context_switcher.h"
#include "async_queue.h"
#include "async_no_threads.h"


#define ASYNC_QUEUE_TAG 'asq0'
#define ASYNC_THREAD_CONTEXT_TAG 'tcx0'


static queue_t *queue = NULL;

static queue_element_t *ended_element = NULL;

static context_t *thread_context = NULL;


static void async_initialize(void) {
	queue = ExAllocatePoolWithTag(PagedPool, sizeof(queue_t), ASYNC_QUEUE_TAG);
	queue_initialize(queue);
	if (thread_context == NULL) {
		queue_put(queue, NULL, NULL);
		queue_element_t *thread_element = queue->head;
		thread_context = (context_t*)ExAllocatePoolWithTag(PagedPool, sizeof(context_t), ASYNC_THREAD_CONTEXT_TAG);
		async_context_prepare_initial(thread_context);
		thread_element->context = thread_context;
	}	
}

void async_queue_task(coroutine_body_t body, void *argument) {
	if (queue == NULL) {
		async_initialize();
		queue_put_first(queue, body, argument);		
	} else {
		queue_put(queue, body, argument);
	}
}

void async_yield_to(coroutine_body_t body, void *argument) {
	if (queue == NULL) {
		async_initialize();
	}
	queue_put_first(queue, body, argument);
	async_yield();
}
	
void async_yield(void) {
	if (ended_element != NULL) {
		async_finalize_context(ended_element->context);
		queue_remove(queue, ended_element);
		ended_element = NULL;
	}
	queue_element_t *next_element = queue_get(queue);
	queue_element_t *current_element = next_element->prev;
	if (current_element == NULL) {
		return;
	} else {
		if (next_element->context == NULL) {
			async_initial_context_switch(current_element->context, next_element);
		} else {
			async_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_return(void) {
	if (ended_element != NULL) {
		async_finalize_context(ended_element->context);
		queue_remove(queue, ended_element);
		ended_element = NULL;
	}
	queue_element_t *next_element = queue_get(queue);
	queue_element_t *current_element = next_element->prev;
	if (current_element == NULL) {
		/* last element in queue */
		queue_remove(queue, next_element);
		async_deinitialize();
		return;
	} else {
		ended_element = current_element;
		if (next_element->context == NULL) {
			async_final_initial_context_switch(current_element->context, next_element);
		} else {
			async_final_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_deinitialize(void) {
	if (queue != NULL) {
		ExFreePoolWithTag(queue, ASYNC_QUEUE_TAG);
		queue = NULL;
	}
	if (thread_context != NULL) {
		ExFreePoolWithTag(thread_context, ASYNC_QUEUE_TAG);
		thread_context = NULL;
	}
	if (ended_element != NULL) {
		async_finalize_context(ended_element->context);
		queue_release_element(ended_element);
		ended_element = NULL;
	}
}
