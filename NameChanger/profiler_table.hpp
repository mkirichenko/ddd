#ifndef PROFILER_TABLE_HPP
#define PROFILER_TABLE_HPP

extern "C" {
	#include "profiler_message_tree.h"
}

#define PROFILER_TABLE_SIZE 32


namespace profiler {

struct table_list_t {
	void *key;
	message_tree_t *value;
	struct table_list_t *next;
};
	
class message_tree_table_t {
private:

	int index(void *key);
	
public:
	
	table_list_t *table[PROFILER_TABLE_SIZE];
	
	int size();
	
	message_tree_table_t();
	
	~message_tree_table_t();
	
	message_tree_t *get(void *key);
	
	void put(void *key, message_tree_t *value);
	
};

}


#endif /* PROFILER_TABLE_HPP */