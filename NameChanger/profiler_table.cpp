#include <wdm.h>
#include "profiler_table.hpp"


#define PROFILER_TABLE_TAG 'prtt'

#define PROFILER_TABLE_VALUE_TAG 'ptvt'

namespace profiler {
	
message_tree_table_t::message_tree_table_t() {
	int i;
	for (i = 0; i < PROFILER_TABLE_SIZE; ++i) {
		this->table[i] = NULL;
	}
}

message_tree_table_t::~message_tree_table_t() {
	int i;
	table_list_t *next;
	table_list_t *prev;
	for (i = 0; i < PROFILER_TABLE_SIZE; ++i) {
		next = table[i];
		while (next != nullptr) {
			prev = next;
			next = next->next;
			ExFreePoolWithTag(prev, PROFILER_TABLE_VALUE_TAG);
		}
	}
}

int message_tree_table_t::index(void *key) {
	return (int)((INT64)key & (PROFILER_TABLE_SIZE - 1));
}

message_tree_t* message_tree_table_t::get(void *key) {
	int i = index(key);
	table_list_t *list = table[i];
	message_tree_t *value = nullptr;
	while (list != nullptr) {
		if (list->key == key) {
			value = list->value;
			break;
		}
		list = list->next;
	}
	return value;
}

int message_tree_table_t::size() {
	return PROFILER_TABLE_SIZE;
}

void message_tree_table_t::put(void *key, message_tree_t *value) {
	int i = index(key);
	table_list_t *element = (table_list_t*)ExAllocatePoolWithTag(PagedPool, sizeof(table_list_t), PROFILER_TABLE_VALUE_TAG);
	element->key = key;
	element->value = value;
	element->next = nullptr;
	if (table[i] == nullptr) {
		table[i] = element;
	} else {
		table_list_t *list = table[i];
		while (list->next != nullptr) {
			list = list->next;
		}
		list->next = element;
	}
}

}