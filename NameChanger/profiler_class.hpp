#ifndef	PROFILER_CLASS_HPP
#define PROFILER_CLASS_HPP


#include <wdm.h>
#include "profiler_table.hpp"


extern "C" {
	#include "profiler_exporter.h"
	#include "profiler_message_tree.h"
	#include "profiler_message_list.h"
}


#define TRACE() profiler::trace(__LINE__, __FILE__, __FUNCTION__)


namespace profiler {
	
class trace {
public:
	
	trace(int line, const char *file, const char *note);
	
	~trace();
	
private:
	
	message_tree_t *tree;
	
	message_tree_node_t *message;
	
};


class profiler {
public:
		
	static void init(void);

	static message_tree_t *get_thread_tree();

	static void add_message(message_type_t type, char *message, INT64 time, HANDLE thread_id);
	
	static void export_to_file(const wchar_t *filename, export_type_t export_type = TEXT);

private:

	static message_tree_table_t trees_table;

	static message_list_t message_list;
	
};


}


#endif