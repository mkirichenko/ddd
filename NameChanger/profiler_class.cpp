#include <ntddk.h>
#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler_table.hpp"
#include "profiler_class.hpp"


extern "C" {
	#include "profiler_exporter.h"
	#include "profiler_message_tree.h"
	#include "profiler.h"
};


#define PROFILER_MESSAGE_BUFFER_TAG 'pmbt'

#define PROFILER_MESSAGE_TREE_TAG 'pmtt'

#define PROFILER_MESSAGE_BUFFER_SIZE 1024


extern "C" {
	int __cdecl atexit(void (*f)(void)) {
		UNREFERENCED_PARAMETER(f);
		return 0;
	}
};


namespace profiler {

void profiler::init() {
	profiler_initialize();
	trees_table = message_tree_table_t();
	profiler_message_list_initialize(&message_list);
}

message_tree_table_t profiler::trees_table;

message_list_t profiler::message_list;

trace::trace(int line, const char *file, const char *note) {
	message_tree_t *thread_tree = profiler::get_thread_tree();
	message_tree_node_t *tree_message = profiler_message_tree_add(thread_tree, note);
	tree_message->line = line;
	tree_message->file = file;
	profiler_thread_all_time_begin(&tree_message->cpu_time, &tree_message->real_time);

	this->tree = thread_tree;
	this->message = tree_message;
}

trace::~trace() {
	profiler_thread_all_time_end(&message->cpu_time, &message->real_time);
	profiler_message_tree_return(tree, message);
}

static message_tree_t *create_message_tree() {
	message_tree_t *new_tree = (message_tree_t*)ExAllocatePoolWithTag(PagedPool, sizeof(message_tree_t), PROFILER_MESSAGE_TREE_TAG);
	profiler_message_tree_initialize(new_tree);
	return new_tree;
}

message_tree_t *profiler::get_thread_tree() {
	void *thread_id = PsGetCurrentThreadId();
	message_tree_t *thread_tree = trees_table.get(thread_id);
	if (thread_tree == nullptr) {
		thread_tree = create_message_tree();
		trees_table.put(thread_id, thread_tree);
	}
	return thread_tree;
}

void profiler::add_message(message_type_t type, char *message, INT64 time, HANDLE thread_id) {
	INT64 timestamp;
	KeQuerySystemTime((PLARGE_INTEGER)&timestamp);
	message_list_node_t *new_node = profiler_message_list_add(&message_list, message);
	new_node->timestamp = timestamp;
	new_node->type = type;
	new_node->time = time;
	new_node->thread_id = thread_id;
}

static void export_tree_node_to_file(HANDLE log_file, message_tree_node_t *node, int level, export_type_t export_type) {
	profiler_exporter_thread_tree_node_header(log_file, export_type, node, level);
	message_tree_child_t *children = node->children;
	while (children != nullptr) {
		export_tree_node_to_file(log_file, children->child, level + 1, export_type);
		children = children->next;
	}
	profiler_exporter_thread_tree_node_footer(log_file, export_type, node, level);
}

static void export_tree_to_file(HANDLE log_file, message_tree_t *tree, export_type_t export_type) {
	profiler_exporter_thread_tree_header(log_file, export_type);
	export_tree_node_to_file(log_file, tree->head, 0, export_type);
	profiler_exporter_thread_tree_footer(log_file, export_type);
}

static void export_trees_to_file(HANDLE log_file, message_tree_table_t &table, export_type_t export_type) {
	table_list_t *table_element;
	int is_tree_exported = 0;
	for (int i = 0; i < table.size(); ++i) {
		table_element = table.table[i];
		while (table_element != nullptr) {
			if (!is_tree_exported) {
				is_tree_exported = 1;
				profiler_exporter_before_thread_trees(log_file, export_type);
			}
			export_tree_to_file(log_file, table_element->value, export_type);
			table_element = table_element->next;
		}
	}
	if (is_tree_exported) {
		profiler_exporter_after_thread_trees(log_file, export_type);
	}
}

void export_messages_to_file(HANDLE log_file, message_list_t &list, export_type_t export_type) {
	int message_number = 0;
	message_list_node_t *current_element = list.head;
	while (current_element != NULL) {
		if (!message_number) {
			profiler_exporter_before_messages(log_file, export_type);
		}
		profiler_exporter_message_header(log_file, export_type, current_element, message_number);
		profiler_exporter_message_footer(log_file, export_type, current_element, message_number);
		current_element = current_element->next;
		++message_number;
	}
	if (message_number) {
		profiler_exporter_after_messages(log_file, export_type);
	}
}

void profiler::export_to_file(const wchar_t *filename, export_type_t export_type) {
	HANDLE log_file = profiler_create_log_file(filename);

	profiler_exporter_header(log_file, export_type);
	export_trees_to_file(log_file, trees_table, export_type);
	export_messages_to_file(log_file, message_list, export_type);
	profiler_exporter_footer(log_file, export_type);

	profiler_close_log_file(log_file);
}



}
