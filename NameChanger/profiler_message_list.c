#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler_message_list.h"


#define PROFILER_MESSAGE_LIST_NODE_TAG 'pmnt'

#define PROFILER_MAX_NOTE_SIZE 256


void profiler_message_list_initialize(message_list_t *list) {
	list->head = NULL;
	list->last = NULL;
}

void profiler_message_list_deinitialize(message_list_t *list) {
	message_list_node_t *next_element = list->head;
	message_list_node_t *current_element;
	if (next_element != NULL) {
		current_element = next_element;
		next_element = next_element->next;
		ExFreePoolWithTag(current_element, PROFILER_MESSAGE_LIST_NODE_TAG);
	}
	list->head = NULL;
	list->last = NULL;
}

message_list_node_t *profiler_message_list_add(message_list_t *list, char *note) {
	size_t note_size;
	RtlStringCbLengthA(note, PROFILER_MAX_NOTE_SIZE, &note_size);
	note_size += 1;
	size_t node_size = sizeof(message_list_node_t) + note_size;
	message_list_node_t *node = (message_list_node_t*)ExAllocatePoolWithTag(PagedPool, node_size, PROFILER_MESSAGE_LIST_NODE_TAG);
	char *note_copy = (char*)node + sizeof(message_list_node_t);
	RtlStringCbCopyA(note_copy, note_size, note);
	node->message = note_copy;
	node->next = NULL;

	if (list->last != NULL) {
		list->last->next = node;
		list->last = node;
	} else {
		list->head = node;
		list->last = node;
	}
	return node;
}
