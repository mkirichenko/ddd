#ifndef PROFILER_MESSAGE_LIST_H
#define PROFILER_MESSAGE_LIST_H

#include <ntdef.h>

typedef enum message_type_t {
	TOTAL_TIME,
	TOTAL_WAIT_TIME,
	TIMESTAMP,
	TIME_INTERVAL,
	THREAD_WAIT_TIME_INTERVAL,
	THREAD_CPU_TIME_INTERVAL,
	THREAD_TOTAL_WAIT_TIME,
	THREAD_TOTAL_CPU_TIME,
	THREAD_TOTAL_TIME
} message_type_t;

typedef struct message_list_node_t {
	struct message_list_node_t *next;
	INT64 timestamp;
	HANDLE thread_id;
	const char *message;
	INT64 time;
	message_type_t type;
} message_list_node_t;

typedef struct message_list_t {
	message_list_node_t *head;
	message_list_node_t *last;
} message_list_t;


void profiler_message_list_initialize(message_list_t *list);

void profiler_message_list_deinitialize(message_list_t *list);

message_list_node_t *profiler_message_list_add(message_list_t *list, char *message);

#endif /* PROFILER_MESSAGE_LIST_H */
