#include "profiler_class.hpp"

extern "C" {
	#include "profiler.h"
};

extern "C" {
	#include "profiler_class_c.h"

	void profiler_c_init(void) {
		profiler::profiler::init();
	}

	profiler_trace_t profiler_c_create_trace_obj(int line, const char *file, const char *note) {
		message_tree_t *thread_tree = profiler::profiler::get_thread_tree();
		message_tree_node_t *tree_message = profiler_message_tree_add(thread_tree, note);
		tree_message->line = line;
		tree_message->file = file;
		profiler_thread_all_time_begin(&tree_message->cpu_time, &tree_message->real_time);
		
		return { thread_tree, tree_message };
	}

	void profiler_c_terminate_trace_obj(profiler_trace_t t) {
		message_tree_t *tree = t.tree;
		message_tree_node_t *message = t.message;
		profiler_thread_all_time_end(&message->cpu_time, &message->real_time);
		profiler_message_tree_return(tree, message);
	}

	void profiler_c_add_message(message_type_t type, char *message, INT64 time, HANDLE thread_id) {
		profiler::profiler::add_message(type, message, time, thread_id);
	}

	void profiler_c_export(const wchar_t *file) {
		profiler::profiler::export_to_file(file, HTML);
	}

}
