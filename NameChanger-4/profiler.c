#include <ntddk.h>
#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler.h"


#define PROFILER_MESSAGE_NODE_TAG 'pmnt'

#define PROFILER_THREAD_START_TIME_TAG 'ptst'

#define PROFILER_FILENAME_BUFFER 'pflb'

#define PROFILER_MESSAGE_BUFFER_SIZE 1024


typedef enum message_type_t {
	TOTAL_TIME,
	TIMESTAMP,
	TIME_INTERVAL,
	THREAD_WAIT_TIME_INTERVAL,
	THREAD_CPU_TIME_INTERVAL,
	THREAD_TOTAL_WAIT_TIME,
	THREAD_TOTAL_CPU_TIME,
	THREAD_TOTAL_TIME
} message_type_t;

typedef struct message_list_node_t {
	struct message_list_node_t *next;
	HANDLE thread_id;
	const char *message;
	INT64 time;
	message_type_t type;
} message_list_node_t;

typedef struct thread_start_time_t {
	HANDLE thread_id;
	LARGE_INTEGER start_time;
	struct thread_start_time_t *next;
} thread_start_time_t;


static message_list_node_t *message_list = NULL;

static LARGE_INTEGER start_process_time;

static thread_start_time_t *start_time_list = NULL;


static void profiler_add_thread_start_time(HANDLE thread_id, LARGE_INTEGER start_time) {
	thread_start_time_t *thread_time = (thread_start_time_t*)ExAllocatePoolWithTag(PagedPool, sizeof(thread_start_time_t), PROFILER_THREAD_START_TIME_TAG);
	thread_time->thread_id = thread_id;
	thread_time->start_time = start_time;
	if (start_time_list == NULL) {
		start_time_list = thread_time;
	} else {
		thread_start_time_t *element = start_time_list;
		while (element->next != NULL) {
			element = element->next;
		}
		element->next = thread_time;
	}
}

static PLARGE_INTEGER profiler_get_thread_start_time(HANDLE thread_id) {
	PLARGE_INTEGER start_time = NULL;
	thread_start_time_t *element = start_time_list;
	while (element != NULL) {
		if (element->thread_id == thread_id) {
			start_time = &element->start_time;
			break;
		}
		element = element->next;
	}
	return start_time;
}

static void profiler_add_message_to_list(message_type_t type, const char *message, INT64 time, HANDLE thread_id) {
	message_list_node_t *new_message = (message_list_node_t*)ExAllocatePoolWithTag(PagedPool, sizeof(message_list_node_t), PROFILER_MESSAGE_NODE_TAG);
	new_message->type = type;
	new_message->message = message;
	new_message->time = time;
	new_message->thread_id = thread_id;
	new_message->next = NULL;
	if (message_list != NULL) {
		message_list_node_t *last_message = message_list;
		while (last_message->next != NULL) {
			last_message = last_message->next;
		}
		last_message->next = new_message;
	} else {
		message_list = new_message;
	}
}

HANDLE profiler_create_log_file(const wchar_t *filename) {
	HANDLE file_handle;
	IO_STATUS_BLOCK status_block;
	NTSTATUS status;
	UNICODE_STRING file_name;
	OBJECT_ATTRIBUTES file_attributes;

	const wchar_t *filename_prefix = L"\\DosDevices\\C:\\";
	size_t fp_size;
	RtlStringCbLengthW(filename_prefix, 1024, &fp_size);
	size_t f_size;
	RtlStringCbLengthW(filename, 1024, &f_size);
	size_t fbuffer_size = fp_size + f_size + sizeof(wchar_t);
	wchar_t *filename_buffer = (wchar_t*)ExAllocatePoolWithTag(PagedPool, fbuffer_size, PROFILER_FILENAME_BUFFER);
	RtlInitEmptyUnicodeString(&file_name, filename_buffer, (USHORT)fbuffer_size);
	RtlUnicodeStringCatString(&file_name, filename_prefix);
	RtlAppendUnicodeToString(&file_name, filename);

	InitializeObjectAttributes(&file_attributes, &file_name, OBJ_KERNEL_HANDLE | OBJ_CASE_INSENSITIVE, NULL, NULL);
	ACCESS_MASK file_access_mask = STANDARD_RIGHTS_WRITE | FILE_WRITE_DATA;
	status = ZwCreateFile(&file_handle, 
		file_access_mask, 
		&file_attributes, 
		&status_block, 
		NULL, 
		FILE_ATTRIBUTE_NORMAL, 
		0, 
		FILE_OVERWRITE_IF,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 
		0);
	if (status != STATUS_SUCCESS) {
		/* bad */
	}
	return file_handle;
}

void profiler_close_log_file(HANDLE file) {
	ZwClose(file);
}

void profiler_write_message_to_log_file(HANDLE log_file, char *message, size_t size) {
	NTSTATUS status;
	IO_STATUS_BLOCK status_block;
	PVOID buffer = (PVOID)message;
	ULONG length = (ULONG)size;
	status = ZwWriteFile(log_file, NULL, NULL, NULL, &status_block, buffer, length, NULL, NULL);
	if (!NT_SUCCESS(status)) {
		/* bad */
	}
}

void profiler_initialize(void) {
	KeQuerySystemTime(&start_process_time);
	profiler_thread_initialize();
}

void profiler_deinitialize(void) {
	thread_start_time_t *next_element = start_time_list;
	thread_start_time_t	*current_element;
	while (next_element != NULL) {
		current_element = next_element;
		next_element = current_element->next;
		ExFreePoolWithTag(current_element, PROFILER_THREAD_START_TIME_TAG);
	}
	message_list_node_t *next_message = message_list;
	message_list_node_t *current_message;
	while (next_message != NULL) {
		current_message = next_message;
		next_message = next_message->next;
		ExFreePoolWithTag(current_message, PROFILER_MESSAGE_NODE_TAG);
	}
}

void profiler_thread_initialize(void) {
	HANDLE thread_id = PsGetCurrentThreadId();
	LARGE_INTEGER system_time;
	KeQuerySystemTime(&system_time);
	profiler_add_thread_start_time(thread_id, system_time);
}

void profiler_total_time(void) {
	LARGE_INTEGER end_process_time;
	KeQuerySystemTime(&end_process_time);
	/* convert to us */
	INT64 total_time = (end_process_time.QuadPart - start_process_time.QuadPart) / 10;

	profiler_add_message_to_list(TOTAL_TIME, "Total time", total_time, NULL);
}

void profiler_timestamp(void) {
	INT64 system_time;
	KeQuerySystemTime((PLARGE_INTEGER)&system_time);

	profiler_add_message_to_list(TIMESTAMP, "Timestamp", system_time, NULL);
}

INT64 profiler_time_begin(void) {
	INT64 begin_time;
	KeQuerySystemTime((PLARGE_INTEGER)&begin_time);
	return begin_time;
}

void profiler_time_end(INT64 *begin_time) {
	INT64 end_time;
	KeQuerySystemTime((PLARGE_INTEGER)&end_time);
	/* convert to us */
	INT64 real_time = (end_time - *begin_time) / 10;

	profiler_add_message_to_list(TIME_INTERVAL, "Time interval", real_time, NULL);
}

void profiler_thread_all_time_begin(INT64 *cpu_time, INT64 *real_time) {
	KeQuerySystemTime((PLARGE_INTEGER)real_time);
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	*cpu_time = (user_time + kernel_time) * clock_increment;
}

void profiler_thread_all_time_end(INT64 *cpu_time, INT64 *real_time) {
	LARGE_INTEGER end_real_time;
	KeQuerySystemTime(&end_real_time);
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 end_cpu_time = (user_time + kernel_time) * clock_increment;
	*real_time = (end_real_time.QuadPart - *real_time) / 10;
	*cpu_time = (end_cpu_time - *cpu_time) / 10;
}

time_pair_t profiler_thread_wait_time_begin() {
	time_pair_t begin_time;
	KeQuerySystemTime((PLARGE_INTEGER)&begin_time.t1);
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	begin_time.t2 = (user_time + kernel_time) * clock_increment;
	return begin_time;
}

void profiler_thread_wait_time_end(time_pair_t *begin_time) {
	LARGE_INTEGER end_real_time;
	KeQuerySystemTime(&end_real_time);
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 end_cpu_time = (user_time + kernel_time) * clock_increment;
	INT64 real_time = end_real_time.QuadPart - begin_time->t1;
	INT64 cpu_time = end_cpu_time - begin_time->t2;
	/* convert to us */
	INT64 wait_time = (real_time - cpu_time) / 10;

	profiler_add_message_to_list(THREAD_WAIT_TIME_INTERVAL, "Thread wait time interval", wait_time, PsGetCurrentThreadId());
}

INT64 profiler_thread_cpu_time_begin(void) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 begin_cpu_time = (user_time + kernel_time) * clock_increment;
	return begin_cpu_time;
}

void profiler_thread_cpu_time_end(INT64 *begin_cpu_time) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 end_cpu_time = (user_time + kernel_time) * clock_increment;
	/* convert to us */
	INT64 cpu_time = (end_cpu_time - *begin_cpu_time) / 10;

	profiler_add_message_to_list(THREAD_CPU_TIME_INTERVAL, "Thread CPU time interval", cpu_time, PsGetCurrentThreadId());
}

void profiler_thread_total_wait_time(void) {
	PKTHREAD current_thread = KeGetCurrentThread();
	HANDLE thread_id = PsGetCurrentThreadId();
	PLARGE_INTEGER start_real_time = profiler_get_thread_start_time(thread_id);
	if (start_real_time == NULL) {
		start_real_time = &start_process_time;
	}
	LARGE_INTEGER end_real_time;
	KeQuerySystemTime(&end_real_time);
	INT64 real_time = end_real_time.QuadPart - start_real_time->QuadPart;

	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 cpu_time = (user_time + kernel_time) * clock_increment;
	/* convert to us */
	INT64 thread_wait_time = (real_time - cpu_time) / 10;

	profiler_add_message_to_list(THREAD_TOTAL_WAIT_TIME, "Thread total wait time", thread_wait_time, thread_id);
}

void profiler_thread_total_cpu_time(void) {
	PKTHREAD current_thread = KeGetCurrentThread();
	ULONG user_time;
	ULONG kernel_time = KeQueryRuntimeThread(current_thread, &user_time);
	ULONG clock_increment = KeQueryTimeIncrement();
	INT64 cpu_time = (user_time + kernel_time) * clock_increment;
	/* convert to us */
	cpu_time /= 10;

	profiler_add_message_to_list(THREAD_TOTAL_CPU_TIME, "Thread total CPU time", cpu_time, PsGetCurrentThreadId());
}

void profiler_thread_total_time(void) {
	HANDLE thread_id = PsGetCurrentThreadId();
	PLARGE_INTEGER start_real_time = profiler_get_thread_start_time(thread_id);
	if (start_real_time == NULL) {
		start_real_time = &start_process_time;
	}
	LARGE_INTEGER end_real_time;
	KeQuerySystemTime(&end_real_time);
	INT64 total_time = end_real_time.QuadPart - start_real_time->QuadPart;
	/* convert to us */
	total_time /= 10;

	profiler_add_message_to_list(THREAD_TOTAL_TIME, "Thread total time", total_time, thread_id);
}

void profiler_export_messages(HANDLE log_file) {
	message_list_node_t *message = message_list;
	char message_buffer[PROFILER_MESSAGE_BUFFER_SIZE];
	char *buffer = message_buffer;
	size_t message_size;
	unsigned int message_number = 0;

	RtlStringCbPrintfA(buffer, PROFILER_MESSAGE_BUFFER_SIZE, "\nMessages\n");
	RtlStringCbLengthA(buffer, PROFILER_MESSAGE_BUFFER_SIZE, &message_size);
	profiler_write_message_to_log_file(log_file, buffer, message_size);
	while (message != NULL) {
		switch (message->type) {
		case TIMESTAMP:
			;
			TIME_FIELDS ts;
			RtlTimeToTimeFields((PLARGE_INTEGER)&message->time, &ts);
			RtlStringCbPrintfA(buffer, PROFILER_MESSAGE_BUFFER_SIZE, "%u %s: %d.%d.%d %d:%d:%d.%d%d\n", message_number, message->message, ts.Day, ts.Month, ts.Year, ts.Hour, ts.Minute, ts.Second, ts.Milliseconds, message->time % 1000);
			break;
		case TOTAL_TIME:
		case TIME_INTERVAL:
			RtlStringCbPrintfA(buffer, PROFILER_MESSAGE_BUFFER_SIZE, "%u %s: %lldus\n", message_number, message->message, message->time);
			break;
		case THREAD_WAIT_TIME_INTERVAL:
		case THREAD_CPU_TIME_INTERVAL:
		case THREAD_TOTAL_WAIT_TIME:
		case THREAD_TOTAL_CPU_TIME:
		case THREAD_TOTAL_TIME:
		default:
			RtlStringCbPrintfA(buffer, PROFILER_MESSAGE_BUFFER_SIZE, "%u [%p] %s: %lldus\n", message_number, message->thread_id, message->message, message->time);
			break;
		}
		RtlStringCbLengthA(buffer, PROFILER_MESSAGE_BUFFER_SIZE, &message_size);
		profiler_write_message_to_log_file(log_file, buffer, message_size);

		++message_number;
		message = message->next;
	}
}
