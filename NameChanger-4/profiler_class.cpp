#include <ntddk.h>
#include <wdm.h>
#include <ntstrsafe.h>
#include "profiler_table.hpp"
#include "profiler_class.hpp"


extern "C" {
	#include "profiler_message_tree.h"
	#include "profiler.h"
};


#define PROFILER_MESSAGE_BUFFER_TAG 'pmbt'

#define PROFILER_MESSAGE_TREE_TAG 'pmtt'

#define PROFILER_MESSAGE_BUFFER_SIZE 1024


extern "C" {
	int __cdecl atexit(void (*f)(void)) {
		UNREFERENCED_PARAMETER(f);
		return 0;
	}
};


namespace profiler {

void profiler::init_table() {
	table = message_tree_table_t();
}

message_tree_table_t profiler::table;

trace::trace(int line, const char *file, const char *note) {
	message_tree_t *thread_tree = profiler::get_thread_tree();
	message_tree_node_t *tree_message = profiler_message_tree_add(thread_tree, note);
	tree_message->line = line;
	tree_message->file = file;
	profiler_thread_all_time_begin(&tree_message->cpu_time, &tree_message->real_time);

	this->tree = thread_tree;
	this->message = tree_message;
}

trace::~trace() {
	profiler_thread_all_time_end(&message->cpu_time, &message->real_time);
	profiler_message_tree_return(tree, message);
}

static message_tree_t *create_message_tree() {
	message_tree_t *new_tree = (message_tree_t*)ExAllocatePoolWithTag(PagedPool, sizeof(message_tree_t), PROFILER_MESSAGE_TREE_TAG);
	profiler_message_tree_initialize(new_tree);
	return new_tree;
}

message_tree_t *profiler::get_thread_tree() {
	void *thread_id = PsGetCurrentThreadId();
	message_tree_t *thread_tree = table.get(thread_id);
	if (thread_tree == nullptr) {
		thread_tree = create_message_tree();
		table.put(thread_id, thread_tree);
	}
	return thread_tree;
}

static void export_tree_node_to_file(HANDLE log_file, message_tree_node_t *node, int level) {
	{
		char export_message_buffer[PROFILER_MESSAGE_BUFFER_SIZE];
		char *buffer = export_message_buffer;
		for (int i = 0; i < level; ++i) {
			buffer[i] = ' ';
		}
		buffer += level;

		size_t buffer_size = PROFILER_MESSAGE_BUFFER_SIZE - level;
		if (node->parent == nullptr) {
			RtlStringCbPrintfA(buffer, buffer_size, "[%p] root\n", (HANDLE)node->message);
		} else {
			RtlStringCbPrintfA(buffer, buffer_size, "%s; at %s:%d; real:%lldus cpu:%lldus\n", node->message, node->file, node->line, node->real_time, node->cpu_time);
		}

		size_t message_size;
		RtlStringCbLengthA(export_message_buffer, PROFILER_MESSAGE_BUFFER_SIZE, &message_size);
		profiler_write_message_to_log_file(log_file, export_message_buffer, message_size);
	}
	message_tree_children_t *children = node->children;
	while (children != nullptr) {
		export_tree_node_to_file(log_file, children->child, level + 1);
		children = children->next;
	}
}

static void export_tree_to_file(HANDLE log_file, message_tree_t *tree) {
	export_tree_node_to_file(log_file, tree->head, 0);
}

void profiler::export_to_file(const wchar_t *filename) {
	HANDLE log_file = profiler_create_log_file(filename);
	
    table_list_t *table_element;
	for (int i = 0; i < table.size(); ++i) {
		table_element = table.table[i];
		while (table_element != nullptr) {
			export_tree_to_file(log_file, table_element->value);
			table_element = table_element->next;
		}
	}
	profiler_export_messages(log_file);
	profiler_close_log_file(log_file);
}



}
