#ifndef PROFILER_CALLS_H
#define PROFILER_CALLS_H


#include "profiler_message_tree.h"


#define C_TRACE() profiler_c_create_trace_obj(__LINE__, __FILE__, __FUNCTION__)

#define C_TRACE_END(t) profiler_c_terminate_trace_obj(t)


typedef struct profiler_trace_t {
	message_tree_t *tree;
	message_tree_node_t *message;
} profiler_trace_t;

typedef profiler_trace_t TT;

profiler_trace_t profiler_c_create_trace_obj(int line, const char *file, const char *note);

void profiler_c_terminate_trace_obj(profiler_trace_t t);

void profiler_c_export(const wchar_t *file);

#endif /* PROFILER_CALLS_H */