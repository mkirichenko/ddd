#ifndef	PROFILER_CLASS_HPP
#define PROFILER_CLASS_HPP


#include <wdm.h>
#include "profiler_table.hpp"


extern "C" {
	#include "profiler_message_tree.h"
}


#ifdef PROFILER_ENABLE
#define TRACE() profiler::trace(__LINE__, __FILE__, __FUNCTION__)
#else
#define TRACE()
#endif


namespace profiler {
	
class trace {
public:
	
	trace(int line, const char *file, const char *note);
	
	~trace();
	
private:
	
	message_tree_t *tree;
	
	message_tree_node_t *message;
	
};


class profiler {
public:
		
	static void init_table(void);

	static message_tree_t *get_thread_tree();
	
	static void export_to_file(const wchar_t *filename);

private:

	static message_tree_table_t table;
	
};


}


#endif