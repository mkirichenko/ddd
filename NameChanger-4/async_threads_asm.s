.CODE

async_get_current_stack_pointer_asm PROC FRAME
	.ENDPROLOG
	; arguments:	
	; none
	; return:
	; rax - rsp value
	
	mov rax, rsp
	ret 
	
async_get_current_stack_pointer_asm ENDP


END