#include <ntddk.h>
#include <wdm.h>
#include "async_context_switcher.h"
#include "async_queue.h"
#include "async_threads.h"


#define ASYNC_QUEUE_TAG 'asq2'

#define ASYNC_NUMBER_OF_THREADS 2


static queue_t *queue = NULL;

static queue_t *ended_queue = NULL;

static context_t *thread_context[ASYNC_NUMBER_OF_THREADS];

static HANDLE thread_handle[ASYNC_NUMBER_OF_THREADS];

static HANDLE thread_id[ASYNC_NUMBER_OF_THREADS];

static queue_element_t* taken_element[ASYNC_NUMBER_OF_THREADS];


static int async_get_thread_number(void) {
	int i;
	int thread_number = -1;
	HANDLE current_thread = PsGetCurrentThreadId();
	for (i = 0; i < ASYNC_NUMBER_OF_THREADS; ++i) {
		if (thread_id[i] == current_thread) {
			thread_number = i;
			break;
		}
	}
	return thread_number;
}

static void async_finalize_ended_tasks(void) {
	while (!queue_is_empty(ended_queue)) {
		queue_element_t *ended_element = queue_try_take(ended_queue);
		if (ended_element == NULL) {
			return;
		}
		while (ended_element->context->state != CONTEXT_STATE_ENDED);
		async_finalize_context(ended_element->context);
		queue_release_element(ended_element);
	}
}

static void async_thread_body(void *argument) {
	int thread_number = (int)((uintptr_t)argument);
	thread_id[thread_number] = PsGetCurrentThreadId();

	context_t context;
	async_context_prepare_initial(&context);
	thread_context[thread_number] = &context;

	queue_element_t *element;
	for (;;) {
		async_finalize_ended_tasks();
		element = queue_take(queue);
		while ((element->context != NULL) && (element->context->state != CONTEXT_STATE_WAITING));
		taken_element[thread_number] = element;
		if (element->context == NULL) {
			/* create & switch context to element->context*/
			async_initial_context_switch(thread_context[thread_number], element);
		}
		else {
			/* switch context */
			async_context_switch(thread_context[thread_number], element->context);
		}
	}
}

static void async_initialize(void) {
	queue = (queue_t*)ExAllocatePoolWithTag(PagedPool, sizeof(queue_t), ASYNC_QUEUE_TAG);
	queue_initialize(queue);
	ended_queue = (queue_t*)ExAllocatePoolWithTag(PagedPool, sizeof(queue_t), ASYNC_QUEUE_TAG);
	queue_initialize(ended_queue);

	int i;
	NTSTATUS status;
	OBJECT_ATTRIBUTES thread_attributes;
	UNICODE_STRING thread_name;
	RtlInitUnicodeString(&thread_name, L"coroutines_queue_thread");
	InitializeObjectAttributes(&thread_attributes, &thread_name, OBJ_KERNEL_HANDLE, NULL, NULL);
	for (i = 0; i < ASYNC_NUMBER_OF_THREADS; ++i) {
		thread_context[i] = NULL;
		thread_id[i] = NULL;
		taken_element[i] = NULL;
		void *start_argument = (void*)i;
		status = PsCreateSystemThread(&(thread_handle[i]), 
			DELETE, 
			NULL, //&thread_attributes, 
			NULL, 
			NULL, 
			async_thread_body, 
			start_argument
		);
		if (status != STATUS_SUCCESS) {
			/* bad */
		}
	}
}

void async_queue_task(coroutine_body_t body, void *argument) {
	if (queue == NULL) {
		async_initialize();
	}
	queue_put(queue, body, argument);
}

void async_yield(void) {
	async_finalize_ended_tasks();
	if (queue_is_empty(queue)) {
		return;
	} else {
		int thread_number = async_get_thread_number();
		if (thread_number == -1) {
			/* bad */
		}
		queue_element_t *next_element = queue_take(queue);
		while ((next_element->context != NULL) && (next_element->context->state != CONTEXT_STATE_WAITING));
		queue_element_t *current_element = taken_element[thread_number];
		taken_element[thread_number] = next_element;
		queue_add(queue, current_element);
		if (next_element->context == NULL) {
			async_initial_context_switch(current_element->context, next_element);
		} else {
			async_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_return(void) {
	async_finalize_ended_tasks();
	int thread_number = async_get_thread_number();
	if (thread_number == -1) {
		/* bad */
	}
	if (queue_is_empty(queue)) {
		queue_element_t *current_element = taken_element[thread_number];
		taken_element[thread_number] = NULL;
		/* return element in queue for later stack release */
		queue_add(ended_queue, current_element);
		async_final_context_switch(current_element->context, thread_context[thread_number]);
	} else {
		queue_element_t *next_element = queue_take(queue);
		while ((next_element->context != NULL) && (next_element->context->state != CONTEXT_STATE_WAITING));
		queue_element_t *current_element = taken_element[thread_number];
		taken_element[thread_number] = next_element;
		/* return element in queue for later stack release */
		queue_add(ended_queue, current_element);
		if (next_element->context == NULL) {
			async_final_initial_context_switch(current_element->context, next_element);
		} else {
			async_final_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_deinitialize(void) {
	int i;
	NTSTATUS status;
	for (i = 0; i < ASYNC_NUMBER_OF_THREADS; ++i) {
		status = ZwClose(thread_handle[i]);
		if (status != STATUS_SUCCESS) {
			/* bad */
		}
		if (taken_element[i] != NULL) {
			queue_release_element(taken_element[i]);
		}
	}
	if (queue != NULL) {
		ExFreePoolWithTag(queue, ASYNC_QUEUE_TAG);
		queue = NULL;
	}
}
