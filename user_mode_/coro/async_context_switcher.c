#include <Windows.h>
#include <assert.h>
#include <stdio.h>
#include "async_context_switcher.h"
#include "async_queue.h"


#define ASYNC_CONTEXT_STACK_TAG 'acs2'


static void *async_context_allocate_stack() {
	/* 16Kb */
	return malloc(0x4000);
	//return ExAllocatePoolWithTag(PagedPool, 0x4000, ASYNC_CONTEXT_STACK_TAG);
}

static void async_context_deallocate_stack(void *stack) {
	free(stack);
	//ExFreePoolWithTag(stack, ASYNC_CONTEXT_STACK_TAG);
}

void async_initial_context_switch(context_t *from_context, queue_element_t *queue_element) {
	//from_context->state = CONTEXT_STATE_WAITING;
	void *to_stack = async_context_allocate_stack();
	printf("%d\tinitial\tswitch from %p to %p, stack %p\n", GetCurrentThreadId(), from_context, (char*)to_stack + 0x3fa0, to_stack);
	async_initial_context_switch_asm(from_context, queue_element, to_stack);
}

void async_context_switch(context_t *from_context, context_t *to_context) {
	printf("%d\t\tswitch from %p to %p, stack %p, ip %p\n", GetCurrentThreadId(), from_context, to_context, to_context->stack, to_context->ret_address);
	if (to_context->state == CONTEXT_STATE_WAITING) {
		//from_context->state = CONTEXT_STATE_WAITING;
		to_context->state = CONTEXT_STATE_RUNNING;
		async_context_switch_asm(from_context, to_context);
	} else {
		/* wrong state */
	}
}

void async_final_context_switch(context_t *from_context, context_t *to_context) {
	printf("%d\tfinal\tswitch from %p to %p, stack %p, ip %p\n", GetCurrentThreadId(), from_context, to_context, to_context->stack, to_context->ret_address);
	if (to_context->state == CONTEXT_STATE_WAITING) {
		//from_context->state = CONTEXT_STATE_ENDED;
		to_context->state = CONTEXT_STATE_RUNNING;
		async_final_context_switch_asm(from_context, to_context);
	} else {
		/* wrong state */
	}
}

void async_final_initial_context_switch(context_t *from_context, queue_element_t *queue_element) {
	printf("%d\tfininit\tswitch from %p\n", GetCurrentThreadId(), from_context);
	void *to_stack = async_context_allocate_stack();
	//from_context->state = CONTEXT_STATE_ENDED;
	async_final_initial_context_switch_asm(from_context, queue_element, to_stack);
}

void async_context_prepare_initial(context_t *context) {
	context->stack = NULL;
	context->state = CONTEXT_STATE_RUNNING;
}

void async_finalize_context(context_t *context) {
	if (context->stack != NULL) {
		free(context->stack);
	}
}
