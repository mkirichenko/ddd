#include <Windows.h>
#include "async_context_switcher.h"
#include "async_queue.h"
#include "async_threads.h"


#define ASYNC_QUEUE_TAG 'asq2'


static queue_t *queue = NULL;

static queue_t *ended_queue = NULL;

static context_t *thread_context[2] = {NULL, NULL};

static HANDLE thread_handle[2];

static int thread_id[2];

static queue_element_t* taken_element[2] = {NULL, NULL};


static int async_get_thread_number(void) {
	int i;
	int thread_number = -1;
	/*
	HANDLE current_thread = KeGetCurrentThread();
	for (i = 0; i < 2; ++i) {
		if (current_thread == thread_handle[i]) {
			thread_number = i;
			break;
		}
	}
	return thread_number;
	*/
	int current_thread = GetCurrentThreadId();
	for (i = 0; i < 2; ++i) {
		if (thread_id[i] == current_thread) {
			thread_number = i;
			break;
		}
	}
	return thread_number;
}

static void async_finalize_ended_tasks(void) {
	while (!queue_is_empty(ended_queue)) {
		queue_element_t *ended_element = queue_try_take(ended_queue);
		if (ended_element == NULL) {
			return;
		}
		while (ended_element->context->state != CONTEXT_STATE_ENDED);
		async_finalize_context(ended_element->context);
		queue_release_element(ended_element);
	}
}

static DWORD WINAPI async_thread_body(void *argument) {
	int thread_number = (int)((uintptr_t)argument);
	thread_id[thread_number] = GetCurrentThreadId();

	context_t context;
	async_context_prepare_initial(&context);
	thread_context[thread_number] = &context;

	queue_element_t *element;
	for (;;) {
		async_finalize_ended_tasks();
		element = queue_take(queue);
		/*
		while ((element->context != NULL) && (element->context->state == CONTEXT_STATE_ENDED)) {
			async_finalize_context(element->context);
			queue_release_element(element);
			element = queue_take(queue);
		}
		*/
		while ((element->context != NULL) && (element->context->state != CONTEXT_STATE_WAITING));
		taken_element[thread_number] = element;
		if (element->context == NULL) {
			/* create & switch context to element->context*/
			async_initial_context_switch(thread_context[thread_number], element);
		}
		else {
			/* switch context */
			async_context_switch(thread_context[thread_number], element->context);
		}
	}
}

static void async_initialize(void) {
	queue = malloc(sizeof(queue_t));
	queue_initialize(queue);
	ended_queue = malloc(sizeof(queue_t));
	queue_initialize(ended_queue);

	uintptr_t i;
	for (i = 0; i < 2; ++i) {
		void *start_argument = (void*)i;
		thread_handle[i] = CreateThread(0, 0, async_thread_body, start_argument, 0,0 );
	}
}

void async_queue_task(coroutine_body_t body, void *argument) {
	if (queue == NULL) {
		async_initialize();
	}
	queue_put(queue, body, argument);
}

void async_yield(void) {
	async_finalize_ended_tasks();
	if (queue_is_empty(queue)) {
		return;
	} else {
		int thread_number = async_get_thread_number();
		if (thread_number == -1) {
			/* bad */
		}
		queue_element_t *next_element = queue_take(queue);
		/*
		while ((next_element->context != NULL) && (next_element->context->state == CONTEXT_STATE_ENDED)) {
			printf("%d\t context free %p\n", GetCurrentThreadId(), next_element->context);
			async_finalize_context(next_element->context);
			queue_release_element(next_element);
			if (queue_is_empty(queue)) {
				return;
			} else {
				next_element = queue_take(queue);
			}
		}
		*/
		while ((next_element->context != NULL) && (next_element->context->state != CONTEXT_STATE_WAITING));
		queue_element_t *current_element = taken_element[thread_number];
		taken_element[thread_number] = next_element;
		queue_add(queue, current_element);
		if (next_element->context == NULL) {
			async_initial_context_switch(current_element->context, next_element);
		} else {
			async_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_return(void) {
	async_finalize_ended_tasks();
	int thread_number = async_get_thread_number();
	if (thread_number == -1) {
		/* bad */
	}
	if (queue_is_empty(queue)) {
		queue_element_t *current_element = taken_element[thread_number];
		taken_element[thread_number] = NULL;
		{
			/* return element in queue for later stack release */
			//queue_add(queue, current_element);
		}
		queue_add(ended_queue, current_element);
		async_final_context_switch(current_element->context, thread_context[thread_number]);
	} else {
		queue_element_t *next_element = queue_take(queue);
		/*
		while ((next_element->context != NULL) && (next_element->context->state == CONTEXT_STATE_ENDED)) {
			printf("%d\t context free %p\n", GetCurrentThreadId(), next_element->context);
			async_finalize_context(next_element->context);
			queue_release_element(next_element);
			if (queue_is_empty(queue)) {
				queue_element_t *current_element = taken_element[thread_number];
				taken_element[thread_number] = NULL;
				queue_add(queue, current_element);
				async_final_context_switch(current_element->context, thread_context[thread_number]);
			} else {
				next_element = queue_take(queue);
			}
		}
		*/
		while ((next_element->context != NULL) && (next_element->context->state != CONTEXT_STATE_WAITING));
		queue_element_t *current_element = taken_element[thread_number];
		taken_element[thread_number] = next_element;
		{
			/* return element in queue for later stack release */
			//queue_add(queue, current_element);
		}
		queue_add(ended_queue, current_element);
		if (next_element->context == NULL) {
			async_final_initial_context_switch(current_element->context, next_element);
		} else {
			async_final_context_switch(current_element->context, next_element->context);
		}
	}
}

void async_deinitialize(void) {
	int i;
	NTSTATUS status;
	for (i = 0; i < 2; ++i) {
		status = CloseHandle(thread_handle[i]);
		if (taken_element[i] != NULL) {
			queue_release_element(taken_element[i]);
		}
	}
	if (queue != NULL) {
		free(queue);
		queue = NULL;
	}
}
