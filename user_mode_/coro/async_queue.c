#include <Windows.h>
#include <stdio.h>
#include "async_queue.h"


#define QUEUE_PUT_TAG 'qpt2'


void queue_initialize(queue_t *queue) {
  queue->semaphore = CreateSemaphore(0, 0L, 1L, 0);
  queue->mutex = CreateMutex(0, FALSE, 0);
  queue->head = NULL;
}

int queue_is_empty(queue_t *queue) {
	if (queue->head == NULL) {
		return 1;
	} else {
		return 0;
	}
}

void queue_add(queue_t *queue, queue_element_t *element) {
	WaitForSingleObject(queue->mutex, INFINITE);
	if (queue->head == NULL) {
		element->next = NULL;
		element->prev = NULL;
		queue->head = element;
		ReleaseSemaphore(queue->semaphore, 1L, NULL);
	} else {
		queue_element_t *head = queue->head;
		element->next = head;
		if (head->prev == NULL) {
			head->next = element;
			element->prev = head;
		} else {
			head->prev->next = element;
			element->prev = head->prev;
		}
		head->prev = element;
	}
	ReleaseMutex(queue->mutex);
}

void queue_put(queue_t *queue, coroutine_body_t body, void *argument) {
	queue_element_t *element = (queue_element_t*)malloc(sizeof(queue_element_t));
	element->body = body;
	element->argument = argument;
	element->context = NULL;
	WaitForSingleObject(queue->mutex, INFINITE);
	if (queue->head == NULL) {
		element->next = NULL;
		element->prev = NULL;
		queue->head = element;
		ReleaseSemaphore(queue->semaphore, 1L, NULL);
	} else {
		queue_element_t *head = queue->head;
		element->next = head;
		if (head->prev == NULL) {
			head->next = element;
			element->prev = head;
		} else {
			head->prev->next = element;
			element->prev = head->prev;
		}
		head->prev = element;
	}
	ReleaseMutex(queue->mutex);
}

queue_element_t *queue_try_take(queue_t *queue) {
	WaitForSingleObject(queue->mutex, INFINITE);
	queue_element_t *head = queue->head;
	queue_element_t *next = head->next;
	if (next != NULL) {
		if (next->next == head) {
			next->next = NULL;
			next->prev = NULL;
		}
		else {
			next->prev = head->prev;
			head->prev->next = next;
		}
		queue->head = next;
	}
	else {
		queue->head = NULL;
	}
	head->next = NULL;
	head->prev = NULL;
	ReleaseMutex(queue->mutex);
	return head;
}

queue_element_t *queue_take(queue_t *queue) {
	for (;;) {
		WaitForSingleObject(queue->mutex, INFINITE);
		if (queue->head != NULL) {
			break;
		}
		ReleaseMutex(queue->mutex);
		WaitForSingleObject(queue->semaphore, INFINITE);
	}
	/*
	while (queue->head == NULL) {
		WaitForSingleObject(queue->semaphore, INFINITE);
	}
	WaitForSingleObject(queue->mutex, INFINITE);
	*/

	queue_element_t *head = queue->head;
	queue_element_t *next = head->next;
	if (next != NULL) {
		if (next->next == head) {
			next->next = NULL;
			next->prev = NULL;
		} else {
			next->prev = head->prev;
			head->prev->next = next;
		}
		queue->head = next;
	} else {
		queue->head = NULL;
	}
	head->next = NULL;
	head->prev = NULL;
	ReleaseMutex(queue->mutex);
	return head;
}

queue_element_t *queue_get(queue_t *queue) {
	for (;;) {
		WaitForSingleObject(queue->mutex, INFINITE);
		if (queue->head != NULL) {
			break;
		}
		ReleaseMutex(queue->mutex);
		WaitForSingleObject(queue->semaphore, INFINITE);
	}
	/*
	while (queue->head == NULL) {
		WaitForSingleObject(queue->semaphore, INFINITE);
	}
	WaitForSingleObject(queue->mutex, INFINITE);
	*/
	queue_element_t *head = queue->head;
	if (head->next != NULL) {
		queue->head = head->next;
	}
	ReleaseMutex(queue->mutex);
	return head;
}

void queue_release_element(queue_element_t *element) {
	free(element);
}

void queue_remove(queue_t *queue, queue_element_t *element) {
	WaitForSingleObject(queue->mutex, INFINITE);
	if (queue->head == element) {
		if (queue->head->next == NULL) {
			queue->head = NULL;			
		} else {
			queue_element_t *new_head = queue->head->next;
			if (queue->head->next == queue->head->prev) {
				new_head->next = NULL;
				new_head->prev = NULL;
			} else {
				new_head->prev = queue->head->prev;
				queue->head->prev->next = new_head;				
			}
			queue->head = new_head;
		}
	} else {
		queue_element_t *prev = element->prev;
		queue_element_t *next = element->next;
		if (prev == next) { /* == queue->head */
			queue->head->next = NULL;
			queue->head->prev = NULL;
		} else {
			prev->next = next;
			next->prev = prev;
		}
	}
	free(element);
	ReleaseMutex(queue->mutex);
}

void queue_deinitialize(queue_t *queue) {
	queue_element_t *head = queue->head;
	queue_element_t *current = queue->head;
	queue_element_t *next;
	while (current != NULL && current->next != head) {
		next = current->next;
		free(current);
		current = next;
	}
	queue->head = NULL;
}
