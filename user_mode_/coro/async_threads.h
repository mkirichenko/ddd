#ifndef ASYNC_THREAD_H
#define ASYNC_THREAD_H

#include "async_types.h"

void async_queue_task(coroutine_body_t body, void *argument);
	
void async_yield(void);

void async_return(void);

void async_deinitialize(void);

#endif /* ASYNC_THREAD_H */
