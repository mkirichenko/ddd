#include <windows.h>

extern "C" {
#include "async_context_switcher.h"
#include "async_threads.h"
}

#include <iostream>

extern "C" {
	void coroutine_body_0(void *argument) {
		UNREFERENCED_PARAMETER(argument);
		int a = 10;
		a += 10;
		//  std::cout << __FUNCTION__ << " " << a << std::endl;
		async_yield();
		a += 20;
		//  std::cout << __FUNCTION__ << " " << a << std::endl;
		async_return();

		//  std::cout << __FUNCTION__ << " " << a << std::endl;
	}

	void coroutine_body_1(void *argument) {
		UNREFERENCED_PARAMETER(argument);
		int b = 20;
		b += 20;
		//std::cout << __FUNCTION__ << " " << b << std::endl;
		async_yield();
		b += 40;
		//std::cout << __FUNCTION__ << " " << b << std::endl;
		async_return();

		//  std::cout << __FUNCTION__ << " " << b << std::endl;
	}

	void coroutine_body_2(void *argument) {
		UNREFERENCED_PARAMETER(argument);
		int c = 30;
		c += 30;
		// std::cout << __FUNCTION__ << " " << c << std::endl;
		async_yield();
		c += 60;
		// std::cout << __FUNCTION__ << " " << c << std::endl;
		async_return();
		//  std::cout << __FUNCTION__ << " " << c << std::endl;
	}
}

int main() {
	std::cout << "size of queue_element_t " << sizeof(queue_element_t) << std::endl;
	std::cout << "size of context_t " << sizeof(context_t) << std::endl;
	std::cout << "size of context_registers_t " << sizeof(context_registers_t) << std::endl;

	async_queue_task(coroutine_body_0, NULL);
	async_queue_task(coroutine_body_1, NULL);
	async_queue_task(coroutine_body_2, NULL);

  for (;;) Sleep(1);

  return 0;
}